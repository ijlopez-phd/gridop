package es.siani.gridop.config;

public interface Configuration {
	
	public static String PROP_GRIDOP_URL = "gridop.url";
	public static String PROP_GRIDOP_SERVICES_POINT = "gridop.servicesPoint";
	
	public static String PROP_ASPEM_INIT_PORT = "aspem.initPort";
	public static String PROP_ASPEM_CONTEXT_PATH = "aspem.contextPath";
	public static String PROP_ASPEM_SERVICES_POINT = "aspem.servicesPoint";
	public static String PROP_ASPEM_RESOURCE = "aspem.resource";
	
	public static String PROP_XMPP_URL = "xmpp.server.url";
	public static String PROP_XMPP_PORT = "xmpp.server.port";
	public static String PROP_XMPP_LOGIN = "xmpp.user.login";
	public static String PROP_XMPP_PASS = "xmpp.user.pass";
	public static String PROP_XMPP_RESOURCE = "xmpp.user.resource";
	
	public static String PROP_JADE_DF_PATH = "jade.df.path";

	
	public String get(String propName);
}
