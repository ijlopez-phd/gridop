package es.siani.gridop.config;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import static es.siani.gridop.GridopGlobals.*;

public class ConfigurationLoader {
	
	
	/** File path to be used to read the configuration file if other path is not specified */
	private static String defaultConfigurationFilePath = null;
	
	/** Cache of created configuration files */
	private static HashMap<String, Configuration> littleCache = new HashMap<String, Configuration>(5);
	
	/** File path from which the configuration file is read */
	private String configFilePath = null;
	
	/** List of the valid names for the configuration properties. */
	static String[] validFieldNames;
	
	static {
		Field[] configFields = Configuration.class.getFields();
		ArrayList<String> validList = new ArrayList<String>(); 
		for (int n = 0; n < configFields.length; n++) {
			Field field = configFields[n];
			if (field.getName().startsWith("PROP_")  && ((field.getModifiers() & Modifier.STATIC) != 0)) {
				try {
					validList.add((String)field.get(null));
				} catch (IllegalArgumentException e) {
					throw e;
				} catch (IllegalAccessException e) {
					throw new IllegalArgumentException(e.getCause());
				}
			}
		}

		validFieldNames = validList.toArray(new String[validList.size()]);
	}
	
	

	/**
	 * Constructor.
	 */
	public ConfigurationLoader() {
		return;
	}

	
	/**
	 * Constructor.
	 */
	public ConfigurationLoader(String filePath) {
		this.configFilePath = filePath;
		return;
	}
	
	
	/**
	 * Set the default configuration file.
	 */
	public static void setDefaultConfigurationFile(String filePath) {
		defaultConfigurationFilePath = filePath;
		return;
	}
	
	
	/**
	 * Create a configuration object from a Properties file.
	 */
	public Configuration getConfiguration() {
		
		String filePath = this.getConfigurationFilePath();
		
		// Check if the requested Configuration file is stored in the cache
		Configuration config = littleCache.get(filePath);
		if (config != null) {
			return config;
		}
		
		// Load the properties file
		Properties props = new Properties();
		try {
			InputStream istream;
			if (filePath.equals(INTERNAL_CONFIG_PATH)) {
				// Read the configuration file included in the WAR
				ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
				istream = classLoader.getResourceAsStream(INTERNAL_CONFIG_PATH);
			} else {
				File file = new File(filePath);
				if (!file.canRead()) {
					throw new IllegalArgumentException("The configuration file cannot be read.");
				}
				istream = new FileInputStream(file);
			}
			
			props.load(istream);
		} catch (FileNotFoundException e) {
			throw new IllegalArgumentException("The configuration file cannot be read.");
		} catch (IOException e) {
			throw new IllegalArgumentException("The configuration file may not be well-formed.");
		}
		
		
		config = this.createConfiguration(props);
		
		littleCache.put(filePath, config);
		return config;
	}
	
	
	/**
	 * Get a path from one of the several sources that can
	 * be used to specify the location of the configuration file. 
	 */
	private String getConfigurationFilePath() {
		if (this.configFilePath != null) {
			return this.configFilePath;
		} else if (System.getenv(VAR_ENV_CONFIG_PATH) != null) {
			return System.getenv(VAR_ENV_CONFIG_PATH);
		} else if (System.getProperty(PARAM_SYS_CONFIG_PATH) != null) {
			return System.getProperty(PARAM_SYS_CONFIG_PATH);
		} else if (defaultConfigurationFilePath != null) {
			return defaultConfigurationFilePath;
		}
		
		getLogger().log(Level.WARNING, "Using the internal configuration file.");
		return INTERNAL_CONFIG_PATH;
	}
	
	
	/**
	 * Create a Configuration object from the Properties object.
	 */
	private Configuration createConfiguration(Properties props) {
		
		ConfigurationImpl config = new ConfigurationImpl();
		Enumeration<Object> keys = props.keys();
		while (keys.hasMoreElements()) {
			String propName = (String)keys.nextElement();
			for (int n = 0; n < validFieldNames.length; n++) {
				if (propName.equals(validFieldNames[n])) {
					config.set(propName, props.getProperty(propName, null));
					break;
				}
			}
		}
		
		return config;
	}
	
	
	/**
	 * Logger.
	 */
	private static final Logger getLogger() {
		return Logger.getLogger(ConfigurationLoader.class.getName());
	}	
}
