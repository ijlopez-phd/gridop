package es.siani.gridop;

import com.vaadin.Application;
import com.vaadin.data.Item;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Component;
import com.vaadin.ui.CustomLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.BaseTheme;
import es.siani.gridop.action.AspemManager;
import es.siani.gridop.action.ClientManager;
import es.siani.gridop.action.GridopManager;
import es.siani.gridop.action.MasterDataManager;
import es.siani.gridop.action.ScenarioManager;
import es.siani.gridop.action.SimulationManager;
import es.siani.gridop.gui.AspemsListView;
import es.siani.gridop.gui.ClientsGroupsListView;
import es.siani.gridop.gui.ClientsListView;
import es.siani.gridop.gui.DisplayModeEnum;
import es.siani.gridop.gui.GridOperatorView;
import es.siani.gridop.gui.ScenarioView;
import es.siani.gridop.gui.ScenariosListView;
import es.siani.gridop.gui.SimulationsListView;
import es.siani.gridop.gui.TextView;
import static es.siani.gridop.GridopGlobals.*;
import static es.siani.gridop.injection.InjectionUtils.*;

@SuppressWarnings("serial")
public class GridOperatorApplication extends Application implements ClickListener {

	private static final int IDX_TITLE = 0;
	private static final int IDX_CONTENT = 1;
	
	private static final int LNK_RIGHT = 0;
	private static final int LNK_TOP = 1;
	
	// Managers
	private GridopManager gridOperatorManager;
	private SimulationManager simulationManager;
	private ScenarioManager scenarioManager;
	private AspemManager aspemManager;
	private ClientManager clientManager;	
	private MasterDataManager masterDataManager;
	
	// Views
	private GridOperatorView gridOperatorInfoView = null;
	private ScenariosListView scenariosListView = null;
	private SimulationsListView simulationsListView = null;
	private AspemsListView aspemsListView = null;
	private ClientsListView clientsListView = null;
	private ClientsGroupsListView clientsGroupsListView = null;
	private TextView aboutView = null;
	private TextView contactView = null;
	
	// Links on the right-side bar
	private Button linkGridOperatorInfoView = null;
	private Button linkScenariosListView = null;
	private Button linkSimulationsListView = null;
	private Button linkAspemsListView = null;
	private Button linkClientsListView = null;
	private Button linkClientsGroupsListView = null;
	
	// Links on the top menu
	private Button linkHomeView = null;
	private Button linkAboutView = null;
	private Button linkContactView = null;
	

	/**
	 * Init the application.
	 */
	@Override
	public void init() {
		this.initObjects();
		this.setupMainWindow();
		return;
	}

	
	/**
	 * Set up the injection dependencies.
	 */
	private void initObjects() {
		this.gridOperatorManager = injector().getInstance(GridopManager.class);
		this.simulationManager = injector().getInstance(SimulationManager.class);
		this.scenarioManager = injector().getInstance(ScenarioManager.class);
		this.aspemManager = injector().getInstance(AspemManager.class);
		this.clientManager = injector().getInstance(ClientManager.class);
		this.masterDataManager = injector().getInstance(MasterDataManager.class);
		
		return;
	}
	

	/**
	 * Read and configure the main window.
	 */
	private void setupMainWindow() {
		this.setTheme("signa");

		Window mainWindow = new Window("Grid Operator Application");
		CustomLayout layout = new CustomLayout("signalayout");
		mainWindow.setContent(layout);
		this.setMainWindow(mainWindow);
		
		// Top links
		this.linkHomeView = this.createButtonLinkSection("Home | ", LNK_TOP);
		layout.addComponent(this.linkHomeView, "linkHome");		
		this.linkAboutView = this.createButtonLinkSection("About | ", LNK_TOP);
		layout.addComponent(this.linkAboutView, "linkAbout");
		this.linkContactView = this.createButtonLinkSection("Contact | ", LNK_TOP);
		layout.addComponent(this.linkContactView, "linkContact");		
		
		
		// Right-side links
		this.linkGridOperatorInfoView = this.createButtonLinkSection("Grid Operator", LNK_RIGHT);
		layout.addComponent(this.linkGridOperatorInfoView, "linkOperatorInfo");
		
		this.linkScenariosListView = this.createButtonLinkSection("Scenarios", LNK_RIGHT);
		layout.addComponent(this.linkScenariosListView, "linkScenarios");
		
		this.linkSimulationsListView = this.createButtonLinkSection("Simulations", LNK_RIGHT);
		layout.addComponent(this.linkSimulationsListView, "linkSimulations");
		
		this.linkAspemsListView = this.createButtonLinkSection("ASPEMs", LNK_RIGHT);
		layout.addComponent(this.linkAspemsListView, "linkAspems");
		
		this.linkClientsListView = this.createButtonLinkSection("Clients", LNK_RIGHT);
		layout.addComponent(this.linkClientsListView, "linkClients");
		
		this.linkClientsGroupsListView = this.createButtonLinkSection("Clients groups", LNK_RIGHT);
		layout.addComponent(this.linkClientsGroupsListView, "linkClientsGroups");
		
		// Content of the body
		this.showBodyView(this.getGridOperatorView());

		return;
	}
	
	
	/**
	 * Get the layout of the conent panel.
	 */
	public CustomLayout getContentLayout() {
		return (CustomLayout)this.getMainWindow().getContent();
	}
	

	/**
	 * Get the Home view.
	 */
	private Component[] getHomeView() {
		return getGridOperatorView();
	}
	
	
	/**
	 * Get the Grid Operator Info view in a lazy way.
	 */
	private Component[] getGridOperatorView() {
		if (this.gridOperatorInfoView == null) {
			this.gridOperatorInfoView = new GridOperatorView(this);
		}
		
		return new Component[] {new Label("Grid Operator Info"), this.gridOperatorInfoView};
	}
	
	
	/**
	 * Get the Scenarios view. 
	 */
	private Component[] getScenariosListView() {
		if (this.scenariosListView == null) {
			this.scenariosListView = new ScenariosListView(this);
			this.scenarioManager.addListener(this.scenariosListView);
		} else {
			this.scenariosListView.reloadScenariosList();
		}
		
		return new Component[] {new Label("Scenarios"), this.scenariosListView};
	}
	
	
	/**
	 * Get the Simulations view.
	 */
	private Component[] getSimulationsListView() {
		if (this.simulationsListView == null) {
			this.simulationsListView = new SimulationsListView(this);
		} else {
			this.simulationsListView.reloadSimulationsList();
		}
		
		return new Component[] {new Label("Simulations"), this.simulationsListView};
	}
	
	
	/**
	 * Get the ASPEMs view.
	 */
	private Component[] getAspemsListView() {
		if (this.aspemsListView == null) {
			this.aspemsListView = new AspemsListView(this);
		} else {
			this.aspemsListView.reloadAspemsList();
		}
		
		return new Component[] {new Label("ASPEMs"), this.aspemsListView};
	}
	
	
	/**
	 * Get the Clients view.
	 */
	private Component[] getClientsListView() {
		if (this.clientsListView == null) {
			this.clientsListView = new ClientsListView(this);
			this.clientManager.addListener(this.clientsListView);
		} else {
			this.clientsListView.reloadClientsList();
		}
		
		return new Component[] {new Label("Clients"), this.clientsListView};
	}
	
	
	/**
	 * Get the Clients Groups view.
	 */
	private Component[] getClientsGroupsListView() {
		if (this.clientsGroupsListView == null) {
			this.clientsGroupsListView = new ClientsGroupsListView(this);
		} else {
			this.clientsGroupsListView.reloadList();
		}
		
		return new Component[] {new Label("Clients Groups"), this.clientsGroupsListView};
	}
	
	
	/**
	 * Get the Contact Information view.
	 */
	private Component[] getContactView() {
		if (this.contactView == null) {
			this.contactView = new TextView(CONTACT_RESOURCE);
		}
		
		return new Component[] {new Label("Contact"), this.contactView};
	}
	
	
	/**
	 * Get the About Information view.
	 */
	private Component[] getAboutView() {
		if (this.aboutView == null) {
			this.aboutView = new TextView(ABOUT_RESOURCE);
		}
		
		return new Component[] {new Label("About"), this.aboutView};
	}	
	
	
	/**
	 * Show the Scenarios List view.
	 */
	public void showScenariosListView() {
		this.showBodyView(this.getScenariosListView());
		return;
	}
	
	
	/**
	 * Show the Simulations List view.
	 */
	public void showSimulationsListView() {
		this.showBodyView(this.getSimulationsListView());
		return;
	}
	
	
	/**
	 * Show the ASPEMs List view.
	 */
	public void showAspemsListView() {
		this.showBodyView(this.getAspemsListView());
		return;
	}
	
	
	/**
	 * Show the Clients List view.
	 */
	public void showClientsListView() {
		this.showBodyView(this.getClientsListView());
	}
	
	
	/**
	 * Show the Clients Groups List view.
	 */
	public void showClientsGroupsListView() {
		this.showBodyView(this.getClientsGroupsListView());
	}
	
	
	/**
	 * Show the scenario view.
	 */
	public void showScenarioView(DisplayModeEnum displayMode, Item scenarioItem) {
		this.showBodyView(this.getScenarioView(displayMode, scenarioItem));
		return;
	}
	
	
	/**
	 * Show the Contact view.
	 */
	public void showContactView() {
		this.showBodyView(this.getContactView());
		return;
	}
	
	
	/**
	 * Show the About view.
	 */
	public void showAboutView() {
		this.showBodyView(this.getAboutView());
		return;
	}
	
	
	/**
	 * Show the Home view.
	 */
	public void showHomeView() {
		this.showBodyView(this.getHomeView());
		return;
	}	
	
	
	/**
	 * Get the a Scenario view according to the display mode (edit or new).
	 */
	public Component[] getScenarioView(DisplayModeEnum mode, Item scenarioItem) {
		Label title = new Label();
		if (mode == DisplayModeEnum.NEW) {
			title.setCaption("New scenario");
		} else {
			title.setCaption("Edit scenario");
		}
		
		ScenarioView scenarioView = new ScenarioView(this, mode, scenarioItem);
		return new Component[] {title, scenarioView};
	}
	
	
	/**
	 * Show the body components.
	 */
	public void showBodyView(Component[] items) {
		CustomLayout layout = (CustomLayout) this.getMainWindow().getContent();
		layout.addComponent(items[IDX_TITLE], "contentTitle");
		layout.addComponent(items[IDX_CONTENT], "contentBody");
		return;
	}
	
	
	/**
	 * Get the data provider for managing the Grid Operator item.
	 */
	public GridopManager getGridOperatorManager() {
		return this.gridOperatorManager;
	}
	
	
	/**
	 * Get the data provider for managing the Scenario items.
	 */
	public ScenarioManager getScenarioManager() {
		return this.scenarioManager;
	}
	
	
	/**
	 * Get the object for managing the Simulation items.
	 */
	public SimulationManager getSimulationManager() {
		return this.simulationManager;
	}
	
	
	/**
	 * Get the Aspem manager.
	 */
	public AspemManager getAspemManager() {
		return this.aspemManager;
	}
	
	
	/**
	 * Get the Client manager.
	 */
	public ClientManager getClientManager() {
		return this.clientManager;
	}
	
	
	/**
	 * Get the provider for accessing the tables with master data.
	 */
	public MasterDataManager getMasterDataManager() {
		return this.masterDataManager;
	}


	/**
	 * Handle the events of the links placed at the main window.
	 */	
	public void buttonClick(ClickEvent event) {
		
		Button source = event.getButton();
		
		if (source == this.linkGridOperatorInfoView) {
			this.showBodyView(this.getGridOperatorView());
			return;
			
		} else if (source == this.linkScenariosListView) {
			this.showBodyView(this.getScenariosListView());
			return;
			
		} else if (source == this.linkSimulationsListView) {
			this.showBodyView(this.getSimulationsListView());
			return;
		} else if (source == this.linkAspemsListView) {
			this.showBodyView(this.getAspemsListView());
			return;
		} else if (source == this.linkClientsListView) {
			this.showBodyView(this.getClientsListView());
			return;
		} else if (source == this.linkClientsGroupsListView) {
			this.showBodyView(this.getClientsGroupsListView());
			return;
		} else if (source == this.linkHomeView) {
			this.showHomeView();
		} else if (source == this.linkAboutView) {
			this.showAboutView();
			return;
		} else if (source == this.linkContactView) {
			this.showContactView();
			return;
		}
		
		return;
	}
	
	
	/**
	 * Create a section link 
	 */
	private Button createButtonLinkSection(String caption, int type) {
		Button link = new Button();
		link.setCaption(caption);
		link.setStyleName(BaseTheme.BUTTON_LINK);
		link.addListener(this);
		if (type == LNK_RIGHT) {
			link.addStyleName("ign-navmenu");
		} else if (type == LNK_TOP) {
			link.addStyleName("ign-topmenu");
		}
		
		return link;
	}
}