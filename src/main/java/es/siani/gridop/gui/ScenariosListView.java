package es.siani.gridop.gui;

import java.util.List;
import java.util.logging.Logger;
import org.vaadin.dialogs.ConfirmDialog;
import com.vaadin.data.Container;
import com.vaadin.data.Item;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Layout.MarginInfo;
import com.vaadin.ui.Panel;
import com.vaadin.ui.Table;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.themes.Runo;
import com.vaadin.event.Action;
import es.siani.gridop.GridOperatorApplication;
import es.siani.gridop.GridopException;
import es.siani.gridop.data.DataProviderListener;
import es.siani.gridop.model.Scenario;
import static es.siani.gridop.GridopGlobals.*;

@SuppressWarnings("serial")
public class ScenariosListView 
		extends Panel
		implements ClickListener, Table.ValueChangeListener, Action.Handler, ConfirmDialog.Listener, DataProviderListener<Scenario> {
	
	// Natural property order for Client data (used in tables and forms)
	private static final Object[] NATURAL_COL_ORDER = new Object[] {
		"code", "storeName", "program.name"
	};
	
	// Human readable captions for properties (in same order as NATURAL_COL_ORDER)
	private static final String[] COL_CAPTIONS = new String[] {
		"Code", "Store", "Program"
	};
	
	// Table's action buttons.
	private final Button buttonNew = new Button("New", (ClickListener)this);
	private final Button buttonRemove = new Button("Remove", (ClickListener)this);
	private final Button buttonEdit = new Button("Edit", (ClickListener)this);
	
	// Context-menu's actions.
	private static Action ACTION_NEW = new Action("New");
	private static Action ACTION_EDIT = new Action("Edit");
	private static Action ACTION_DELETE = new Action("Delete");
	private static Action TABLE_ITEM_ACTIONS[] = {ACTION_NEW, ACTION_EDIT, ACTION_DELETE};
	
	/** Reference to the main application object */
	private final GridOperatorApplication app;
	
	/** Table that lists all the scenarios registered with the Grid Operator System */
	private final Table scenariosTable = new Table();
	

	/**
	 * Constructor.
	 */
	public ScenariosListView(final GridOperatorApplication app) {
		super();
		
		this.app = app;
		
		// Add this view as listener of the Scenario data provider
		this.app.getScenarioManager().addListener(this);
		
		// Populate the table's container with the registered Scenarios
		BeanItemContainer<Scenario> container = new BeanItemContainer<Scenario>(Scenario.class);
		try {
			List<Scenario> scenarios = this.app.getScenarioManager().getAllScenarios();
			container.addAll(scenarios);
			container.addNestedContainerProperty("program.name");
		} catch (GridopException ex) {
			getLogger().severe("The list of scenarios could not be read from the database.");
		}
		
		// Main panel without borders
		this.addStyleName(Runo.PANEL_LIGHT);
		
		// Layout of the view
		VerticalLayout layout = new VerticalLayout();
		layout.setSizeFull();
		
		// Table with all scenarios
		this.scenariosTable.setSizeFull();
		this.scenariosTable.setContainerDataSource(container);
		this.scenariosTable.setVisibleColumns(NATURAL_COL_ORDER);
		this.scenariosTable.setColumnHeaders(COL_CAPTIONS);		
		this.scenariosTable.setSelectable(true);
		this.scenariosTable.setImmediate(true);
		this.scenariosTable.setColumnReorderingAllowed(false);
		this.scenariosTable.setMultiSelect(false);
		this.scenariosTable.setPageLength(TABLE_MAX_VISIBLE_ITEMS);
		this.scenariosTable.addListener((Table.ValueChangeListener)this);
		this.scenariosTable.addActionHandler(this);
		layout.addComponent(this.scenariosTable);
		
		// Buttons for actions
		HorizontalLayout buttonsLayout = new HorizontalLayout();
		buttonsLayout.setMargin(new MarginInfo(true, false, true, false));
		buttonsLayout.setSpacing(true);
		buttonsLayout.addComponent(this.buttonNew);
		buttonsLayout.addComponent(this.buttonEdit);
		buttonsLayout.addComponent(this.buttonRemove);
		layout.addComponent(buttonsLayout);
		
		// Some buttons are not enabled until a row is selected
		this.enableButtons(false);
		
		// Set the content of the panel
		this.setContent(layout);
		
		return;
	}
	
	
	/**
	 * Show the form for inserting a new scenario.
	 */
	private void doNewScenarioAction() {
		this.app.showScenarioView(DisplayModeEnum.NEW, null);
		return;
	}
	
	
	/**
	 * Show the form for editing an existing scenario.
	 */
	private void doEditScenarioAction() {
		
		Object selectedValue = this.scenariosTable.getValue();
		if (selectedValue == null) {
			return;
		}
		
		try {
			Item item = this.scenariosTable.getContainerDataSource().getItem(selectedValue);
			this.app.showScenarioView(DisplayModeEnum.EDIT, item);
		} catch (Exception ex) {
			getLogger().severe("Trying to get the item and its id from the Scenarios table.");
		}
		
		return;
	}
	
	
	/**
	 * Remove the scenario selected on the table.
	 */
	private void doRemoveScenarioAction() {
		Object rowId = this.scenariosTable.getValue();
		if (rowId != null) {
			ConfirmDialog.show(
					this.app.getMainWindow(),
					"Please Confirm:",
					"Are you really sure?",
					"I am", "Not quite",
					(ConfirmDialog.Listener)this);
		}
		
		return;
	}
	

	/**
	 * Enable or disable buttons depending on a row is selected.
	 */
	private void enableButtons(boolean enabled) {
		this.buttonEdit.setEnabled(enabled);
		this.buttonRemove.setEnabled(enabled);
		
		return;
	}
	
	
	/**
	 * Handle changes on selected items of the table.
	 */
	public void valueChange(ValueChangeEvent event) {
		boolean enabled = (event.getProperty().getValue() != null)? true : false;
		this.enableButtons(enabled);
		
		return;
	}
	

	/**
	 * Handle the click events related to the action buttons.
	 */
	public void buttonClick(ClickEvent event) {
		Button source = event.getButton();
		
		if (source == this.buttonNew) {
			this.doNewScenarioAction();
		} else if (source == this.buttonEdit){
			this.doEditScenarioAction();
		} else if (source == this.buttonRemove) {
			this.doRemoveScenarioAction();
		}
		
		return;
	}
	

	/**
	 * Return the list of actions of the table's context-menu.
	 */
	public Action[] getActions(Object target, Object sender) {
		return TABLE_ITEM_ACTIONS;
	}

	
	/**
	 * Handle click events on the table's context-menu.
	 */
	public void handleAction(Action action, Object sender, Object target) {
		
		this.scenariosTable.select(target);
		
		if ((action == ACTION_NEW) && (target != null)) {
			this.doNewScenarioAction();
			
		} else if ((action == ACTION_EDIT) && (target != null)) {
			this.doEditScenarioAction();
			
		} else if ((action == ACTION_DELETE) && (target != null)) {
			this.doRemoveScenarioAction();
		}
		
		return;
	}

	
	/**
	 * Handle the closing event of the confirm dialog.
	 */
	public void onClose(ConfirmDialog dialog) {
		if (dialog.isConfirmed()) {
			try {
				Scenario scenario = (Scenario)this.scenariosTable.getValue();
				this.app.getScenarioManager().deleteScenario(scenario.getId());
				this.scenariosTable.getContainerDataSource().removeItem(scenario);
				this.enableButtons(false);
				
			} catch (GridopException ex) {
				getLogger().severe("Trying to delete the Scenario item.");				
			}
		}
		
		return;
	}
	
	
	/**
	 * Reload the list of simulations.
	 */
	@SuppressWarnings("unchecked")
	public void reloadScenariosList() {
		
		BeanItemContainer<Scenario> container = (BeanItemContainer<Scenario>)this.scenariosTable.getContainerDataSource();
		
		try {
			List<Scenario> scenarios = this.app.getScenarioManager().getAllScenarios();
			container.removeAllItems();
			container.addAll(scenarios);
		} catch (GridopException ex) {
			getLogger().severe("Trying to get the list of scenarios.");
		}
		
		return;
	}
	
	
	/**
	 * Logger.
	 */
	private static final Logger getLogger() {
		return Logger.getLogger(ScenariosListView.class.getName());
	}


	/**
	 * Called when an item action is performed on the Scenario Data Provider.
	 */
	public void itemChanged(int actionType, Scenario item) {
		Container container = this.scenariosTable.getContainerDataSource();
		if (actionType == DataProviderListener.GRIDOP_ITEM_SAVED) {
			// A new scenario has been created. It is added to the table's container.
			container.addItem(item);
			
		}		
		return;
	}
}
