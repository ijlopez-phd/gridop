package es.siani.gridop.gui;

import java.util.logging.Logger;

import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.data.util.BeanItem;
import com.vaadin.data.validator.StringLengthValidator;
import com.vaadin.event.FieldEvents.TextChangeEvent;
import com.vaadin.event.FieldEvents.TextChangeListener;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Component;
import com.vaadin.ui.DefaultFieldFactory;
import com.vaadin.ui.Field;
import com.vaadin.ui.Form;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;

import es.siani.gridop.GridOperatorApplication;
import es.siani.gridop.GridopException;
import es.siani.gridop.model.Scenario;
import es.siani.gridop.parser.GridopFileParserProgramImpl;


@SuppressWarnings("serial")
public class ScenarioView extends Form implements ClickListener, TextChangeListener, Property.ValueChangeListener {
	
	/** Visible properties of the form */
	private static final String visibleProperties[] = {"code", "storeName", "program", "description"};
	
	/** Reference to the main application object */
	private final GridOperatorApplication app;
	
	/** Display mode (edition or new) of the form */
	private final DisplayModeEnum displayMode;
	
	/** Actions buttons of the form */
	private final Button buttonSave = new Button("Save", (ClickListener)this);
	private final Button buttonDiscard = new Button("Discard", (ClickListener)this);
	private final Button buttonCreate = new Button("Create", (ClickListener)this);
	private final Button buttonCancel = new Button("Cancel", (ClickListener)this);
	
	/** Item */
	private final Item scenarioItem;
	
	/** Textfield that shows the name of the DRProgram. */
	private TextField programField = new TextField();
	
	/** Name of the program associated to the Scenario in the database. */
	private String savedProgramName = null;
	
	
	/**
	 * Constructor.
	 */
	@SuppressWarnings("unchecked")
	public ScenarioView(final GridOperatorApplication app, DisplayModeEnum displayMode, final Item scenarioItem) {
		
		this.app = app;
		this.displayMode = displayMode;
		
		this.setWriteThrough(false);
		
		// -- Footer buttons
		HorizontalLayout buttonsLayout = new HorizontalLayout();
		buttonsLayout.setSpacing(true);
		if (displayMode == DisplayModeEnum.NEW) {
			buttonsLayout.addComponent(this.buttonCreate);
			buttonsLayout.addComponent(this.buttonCancel);
		} else {
			buttonsLayout.addComponent(this.buttonSave);
			buttonsLayout.addComponent(this.buttonDiscard);
			buttonsLayout.addComponent(this.buttonCancel);
			this.enableButtons(false);
		}
		
		this.setFooter(buttonsLayout);
		
		// Attach the data source to the form.
		BeanItem<Scenario> item;
		if (displayMode == DisplayModeEnum.NEW) {
			Scenario scenario = new Scenario();
			item = new BeanItem<Scenario>(scenario);
		} else {
			item = (BeanItem<Scenario>)scenarioItem;
		}
		this.setItemDataSource(item);
		
		
		this.scenarioItem = item;		
		
		// Fulfill the name of the selected program
		if (this.scenarioItem.getItemProperty("program.name") != null) {
			this.savedProgramName = (String)this.scenarioItem.getItemProperty("program.name").getValue(); 
			programField.setValue(savedProgramName);
		}
		
		// Set the field factory
		this.setFormFieldFactory(new DefaultFieldFactory() {
			@Override
			public Field createField(Item item, Object propertyId, Component uiContext) {
				
				String propName = propertyId.toString();
				
				if (propName.equalsIgnoreCase("CODE")) {
					TextField field = new TextField();
					field.setRequired(true);
					field.setRequiredError("The Code is missing.");
					field.setCaption("Code");
					field.setDescription("Short name that identifies the scenario.");
					field.setNullRepresentation("");
					field.addValidator(new StringLengthValidator(
							"The length of the field must be less than or equal to 64 characters.", 1, 64, false));
					field.addListener((TextChangeListener)ScenarioView.this);
					field.setImmediate(true);
					return field;
					
				} else if (propName.equalsIgnoreCase("STORENAME")) {
					TextField field = new TextField();
					field.setRequired(true);
					field.setRequiredError("The forecasts' store is missing.");					
					field.setRequired(false);
					field.setCaption("Store name");
					field.setDescription("Name of the database table in which forecasts' data is stored..");
					field.setNullRepresentation("");
					field.addValidator(new StringLengthValidator(
							"The length of the field must be less than or equal to 256 characters.", 1, 256, false));
					field.addListener((TextChangeListener)ScenarioView.this);
					field.setImmediate(true);
					return field;
					
				} else if (propName.equalsIgnoreCase("PROGRAM")) {
					UploaderField field = new UploaderField(app, new GridopFileParserProgramImpl());
					field.setCaption("Program");
					field.setDescription("Demand-Response program associated to the Scenario.");
					field.setRequired(true);
					field.setRequiredError("The Program file is missing.");
					field.addListener(ScenarioView.this);
					return field;
					
				} else if (propName.equalsIgnoreCase("DESCRIPTION")) {
					TextArea field = new TextArea();
					field.setRequired(false);
					field.setCaption("Description");
					field.setDescription("Brief description of the scenario.");
					field.addValidator(new StringLengthValidator(
							"The length of the field must be less than 1024 characters.", 0, 1024, false));				
					field.setNullRepresentation("");
					field.setRows(5);
					field.addListener((TextChangeListener)ScenarioView.this);
					field.setImmediate(true);
					return field;
					
				}
				
				return null;
			}			
		});
		
		// Set the visible properties of the data source
		this.setVisibleItemProperties(ScenarioView.visibleProperties);
		
		// Initial state of buttons
		this.enableButtons(false);
		
		
		return;
	}
	
	
	/**
	 * Enable and disable "Save" and "Discard" buttons.
	 */
	private void enableButtons(boolean enabled) {
		this.buttonSave.setEnabled(enabled);
		this.buttonDiscard.setEnabled(enabled);
		
		return;
	}
	
	
	/**
	 * Handle the events related to the changes on the text fields.
	 */
	public void textChange(TextChangeEvent event) {
		if (displayMode == DisplayModeEnum.NEW) {
			return;
		}
		
		this.enableButtons(true);
		
		return;
	}
	
    /**
     * Handle the change events on the combobox.
     */
    @Override
    public void valueChange(Property.ValueChangeEvent event) {
            this.enableButtons(true);
//            this.comboSimulators.select(event.getProperty().getValue());
            return;
    }	
	
	
	/**
	 * Handle the click events related to the buttons of the footer.
	 */
	public void buttonClick(ClickEvent event) {
		
		final Button source = event.getButton();
		
		if ((source == this.buttonSave) || (source == this.buttonCreate)) {
			// Save changes on the form's data
			try {
				this.commit();
				this.app.showScenariosListView();
			} catch (Exception ex) {
				getLogger().severe("Trying to commit the data.");
			} 
			
		} else if (source == this.buttonDiscard) {
			// Discard changes on the form's data
			this.discard();
			this.enableButtons(false);
			
		} else if (source == this.buttonCancel) {
			this.app.showScenariosListView();
			
		}
		
		return;
	}
	
	
	/**
	 * Updates the scenario item with changes since last commit.
	 */
	@SuppressWarnings("unchecked")	
	@Override
	public void commit() {
		// Commit the data entered in the form to the actual item.
		super.commit();
		
		// Commit the changes to the database.
		try {
			BeanItem<Scenario> item = (BeanItem<Scenario>)this.getItemDataSource();
			this.app.getScenarioManager().saveScenario(item.getBean());
		} catch (GridopException ex) {
			getLogger().severe("The scenario couldn't be stored in the database.");
		}
		
		return;
	}
	
	/**
	 * Discard changes since the last commit.
	 */
	@Override
	public void discard() {
		super.discard();
		return;
	}
	
	
	/**
	 * Logger.
	 */
    private static final Logger getLogger() {
        return Logger.getLogger(ScenarioView.class.getName());
    }
}
