package es.siani.gridop.gui;

import static es.siani.gridop.GridopGlobals.*;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Layout.MarginInfo;
import com.vaadin.ui.themes.Runo;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Panel;
import com.vaadin.ui.Table;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.util.BeanItem;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.data.util.NestedMethodProperty;
import com.vaadin.event.Action;
import com.vaadin.data.Container;

import es.siani.gridop.GridOperatorApplication;
import es.siani.gridop.GridopException;
import es.siani.gridop.data.DataProviderListener;
import es.siani.gridop.model.Client;

@SuppressWarnings("serial")
public class ClientsListView
	extends Panel
	implements ClickListener, Table.ValueChangeListener, Action.Handler, DataProviderListener<Client> {
	
	/** Natural property order of the columns */
	private static final Object[] NATURAL_COL_ORDER = new Object[] {
		"code", "xmppAddress", "remoteXmppAddress", "aspem.code"
	};
	
	/** Human readable captions for properties (in same ordes as NATURAL_COL_ORDER) */
	private static final String[] COL_CAPTIONS = new String[] {
		"Code", "XMPP", "ASBox XMPP", "ASPEM"
	};
	
	/** Button for viewing an item of the table */
	private final Button buttonView = new Button("View", (ClickListener)this);
	
	/** Context-menu action for viewing an item of the table */
	private static final Action ACTION_VIEW = new Action("View");
	private static final Action TABLE_ITEM_ACTIONS[] = {ACTION_VIEW};
	
	/** Reference to the main application object */
	private final GridOperatorApplication app;
	
	/** Table that lists all the clients registered with the Grid Operator System */
	private final Table clientsTable = new Table();
	
	
	/**
	 * Constructor.
	 */
	public ClientsListView(final GridOperatorApplication app) {
		super();
		
		this.app = app;
		
		// Populate the table's container with the registered Clients
		BeanItemContainer<Client> container = new BeanItemContainer<Client>(Client.class);
		try {
			List<Client> clients = this.app.getClientManager().getAllClients();
			container.addAll(clients);
			container.addNestedContainerProperty("aspem.code");
		} catch (GridopException ex) {
			getLogger().log(Level.SEVERE, "Trying to get the list of Clients of the database.");
		}
		
		// Main panel without borders
		this.addStyleName(Runo.PANEL_LIGHT);
		
		// Layout of the view
		VerticalLayout layout = new VerticalLayout();
		layout.setSizeFull();
		
		// Table
		this.clientsTable.setSizeFull();
		this.clientsTable.setContainerDataSource(container);
		this.clientsTable.setVisibleColumns(NATURAL_COL_ORDER);
		this.clientsTable.setColumnHeaders(COL_CAPTIONS);
		this.clientsTable.setSelectable(true);
		this.clientsTable.setImmediate(true);
		this.clientsTable.setColumnReorderingAllowed(false);
		this.clientsTable.setMultiSelect(false);
		this.clientsTable.setPageLength(TABLE_MAX_VISIBLE_ITEMS);
		this.clientsTable.addListener((Table.ValueChangeListener)this);
		this.clientsTable.addActionHandler(this);
		layout.addComponent(this.clientsTable);
		
		// Buttons for actions
		HorizontalLayout buttonsLayout = new HorizontalLayout();
		buttonsLayout.setMargin(new MarginInfo(true, false, true, false));
		buttonsLayout.setSpacing(true);
		buttonsLayout.addComponent(this.buttonView);
		layout.addComponent(buttonsLayout);
		
		// Some buttons are not enabled until a row is selected
		this.enableButtons(false);
		
		// Set the content of the panel
		this.setContent(layout);
		
		return;
	}
	
	
	/**
	 * View the detailed description of the ASPEM in a popup.
	 */
	private void doViewClientAction() {
		
		Client client = (Client)this.clientsTable.getValue();
		if (client != null) {
			BeanItem<Client> item = new BeanItem<Client>(client);
			item.addItemProperty("aspem.code", new NestedMethodProperty(item.getBean(), "aspem.code"));
			ClientPopup popup = new ClientPopup(item);
			this.app.getMainWindow().addWindow(popup);
		}
		
		return;
	}
	
	
	/**
	 * Handle events of the table's buttons.
	 */
	public void buttonClick(ClickEvent event) {
		Button source = event.getButton();
		
		if (source == this.buttonView) {
			this.doViewClientAction();
		}
		
		return;
	}
		

	/**
	 * Handle click events on the table's context-menu.
	 */
	public void handleAction(Action action, Object sender, Object target) {
		this.clientsTable.select(target);
		
		if ((action == ACTION_VIEW) && (target != null)) {
			this.doViewClientAction();
		}
		
		return;
	}

	
	/**
	 * Handle changes of the selected items of the table.
	 */
	public void valueChange(ValueChangeEvent event) {
		boolean enabled = (event.getProperty().getValue() != null)? true : false;
		this.enableButtons(enabled);
		
		return;
	}

	
	/**
	 * Return the list of actions of the table's context-menu.
	 */
	public Action[] getActions(Object target, Object sender) {
		return TABLE_ITEM_ACTIONS;
	}	
	
	
	/**
	 * Enable or disable buttons depending on a row is selected.
	 */
	private void enableButtons(boolean enabled) {
		this.buttonView.setEnabled(enabled);
		return;
	}
	
	
	/**
	 * Reload the Clients listed in the table.
	 */
	@SuppressWarnings("unchecked")
	public void reloadClientsList() {
		
		BeanItemContainer<Client> container = (BeanItemContainer<Client>)this.clientsTable.getContainerDataSource();
		
		try {
			List<Client> clients = this.app.getClientManager().getAllClients();
			container.removeAllItems();
			container.addAll(clients);
		} catch (GridopException ex) {
			getLogger().log(Level.SEVERE, "Trying to get list of Clients.");
		}
		
		return;
	}
	
	
	/**
	 * Called when an item action is performed on the Client Data Provider.
	 */
	public void itemChanged(int actionType, Client item) {
		Container container = this.clientsTable.getContainerDataSource();
		if (actionType == DataProviderListener.GRIDOP_ITEM_SAVED) {
			// A new client has been created. It is added to the table's container.
			container.addItem(item);
		} else if (actionType == DataProviderListener.GRIDOP_ITEM_UPDATED) {
			// Refresh the the value of the nested property.
			// It is not done automatically by Vaadin.
			container.getItem(item).getItemProperty("aspem.code").setValue(item.getAspem().getCode());
		}
		
		return;
	}
	
	
	/**
	 * Logger.
	 */
	private static final Logger getLogger() {
		return Logger.getLogger(AspemsListView.class.getName());
	}	

}
