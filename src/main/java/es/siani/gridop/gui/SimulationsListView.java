package es.siani.gridop.gui;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.vaadin.dialogs.ConfirmDialog;

import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.event.Action;
import com.vaadin.event.Action.Handler;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Layout.MarginInfo;
import com.vaadin.ui.Panel;
import com.vaadin.ui.Table;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.Runo;
import es.siani.gridop.GridOperatorApplication;
import es.siani.gridop.GridopException;
import static es.siani.gridop.GridopGlobals.*;
import es.siani.gridop.model.Simulation;

@SuppressWarnings("serial")
public class SimulationsListView extends Panel implements ClickListener, Handler, ValueChangeListener {
	
	// Natural property order for Client data (used in tables and forms)
	private static final Object[] NATURAL_COL_ORDER = new Object[] {
		"scenario.code", "started", "stopped", "scenario.program.name"
	};

	// Human readable captions for properties (in same order as NATURAL_COL_ORDER)
	private static final String[] COL_CAPTIONS = new String[] {
		"Scenario", "Started", "Stopped", "Program"
	};
	
	/** Reference to the main application object */
	private final GridOperatorApplication app;
	
	/** Table that lists all the simulations that are registered */
	private final Table simulationsTable;
	
	// Table's action buttons.
	private final Button buttonDelete = new Button("Delete", (ClickListener)this);
	private final Button buttonDeleteFinished = new Button("Delete finished", (ClickListener)this);
	
	// Context-menu's actions.
	private static Action ACTION_DELETE = new Action("Delete");
	private static Action ACTION_DELETE_FINISHED = new Action("Delete finished");
	private static Action TABLE_ITEM_ACTIONS[] = {ACTION_DELETE, ACTION_DELETE_FINISHED};
	
	
	/**
	 * Constructor.
	 */
	public SimulationsListView(final GridOperatorApplication app) {
		super();
		
		this.app = app;
		
		// Populate the table's container
		BeanItemContainer<Simulation> container = new BeanItemContainer<Simulation>(Simulation.class);
		container.addNestedContainerProperty("scenario.code");
		container.addNestedContainerProperty("scenario.program.name");		
		try {
			List<Simulation> simulations = this.app.getSimulationManager().getAllSimulations();
			container.addAll(simulations);
		} catch (GridopException ex) {
			getLogger().log(Level.SEVERE, "Trying to get the list of simulations.");
		}
		
		// Main panel without borders
		this.addStyleName(Runo.PANEL_LIGHT);
		
		// Layout of the Simulations view
		VerticalLayout layout = new VerticalLayout();
		layout.setSizeFull();
		
		// Instance of the table.
		// The method responsible for formatting the values of the cells is overrided in
		// order to change the representation of the dates.
		this.simulationsTable = new Table() {
			@Override
			protected String formatPropertyValue(Object rowId, Object colId, Property property) {
				Object propValue = property.getValue();
				if (propValue instanceof Date) {
					return new SimpleDateFormat("dd/MM/yyyy  HH:mm:ss").format((Date)propValue);
				}
				
				return super.formatPropertyValue(rowId, colId, property);
			}
		};
		
		// Table with all scenarios
		this.simulationsTable.setSizeFull();
		this.simulationsTable.setContainerDataSource(container);
		this.simulationsTable.setVisibleColumns(NATURAL_COL_ORDER);
		this.simulationsTable.setColumnHeaders(COL_CAPTIONS);		
		this.simulationsTable.setSelectable(true);
		this.simulationsTable.setImmediate(true);
		this.simulationsTable.setColumnReorderingAllowed(true);
		this.simulationsTable.setMultiSelect(false);
		this.simulationsTable.setPageLength(TABLE_MAX_VISIBLE_ITEMS);
		this.simulationsTable.addListener((Table.ValueChangeListener)this);
		this.simulationsTable.addActionHandler(this);
		layout.addComponent(this.simulationsTable);
		
		// Buttons for actions
		HorizontalLayout buttonsLayout = new HorizontalLayout();
		buttonsLayout.setMargin(new MarginInfo(true, false, true, false));
		buttonsLayout.setSpacing(true);
		buttonsLayout.addComponent(this.buttonDelete);
		buttonsLayout.addComponent(this.buttonDeleteFinished);
		layout.addComponent(buttonsLayout);
		
		// Some buttons are not enabled until a row is selected
		this.enableButtons(false);
		
		// Set the content of the panel
		this.setContent(layout);
		
		return;		
	}
	
	
	/**
	 * Reload the list of simulations.
	 */
	@SuppressWarnings("unchecked")
	public void reloadSimulationsList() {
		
		BeanItemContainer<Simulation> container = 
				(BeanItemContainer<Simulation>) this.simulationsTable.getContainerDataSource();
		
		try {
			List<Simulation> simulations = this.app.getSimulationManager().getAllSimulations();
			container.removeAllItems();
			container.addAll(simulations);
		} catch (GridopException ex) {
			getLogger().log(Level.SEVERE, "Trying to get the list of simulations.");
		}
		
		return;
	}
	
	
	/**
	 * Show the popup for deleting a Simulation item.
	 */
	private void doDeleteSimulationAction() {
		
		Simulation simulation = (Simulation)this.simulationsTable.getValue();
		if (simulation == null) {
			return;
		}
		ConfirmDialog.show(
				this.app.getMainWindow(),
				"Please confirm:",
				"Are you really sure?",
				"I am", "Not quite",
				new ConfirmDialog.Listener() {
					public void onClose(ConfirmDialog dialog) {
						if (! dialog.isConfirmed()) {
							return;
						}
						SimulationsListView parent = SimulationsListView.this; 
						try {
							Simulation simulation = (Simulation)parent.simulationsTable.getValue();
							parent.app.getSimulationManager().deleteSimulation(simulation.getId());
							parent.simulationsTable.getContainerDataSource().removeItem(simulation);
							parent.enableButtons(false);
						} catch (GridopException ex) {
							getLogger().log(Level.SEVERE, "The simulation could not be removed from the table.", ex);
						}
					}
				});
		
		return;
	}
	
	
	/**
	 * Show the popup for deleting all Simulation items.
	 */
	private void doDeleteFinishedSimulationsAction() {
		
		ConfirmDialog.show(
				this.app.getMainWindow(),
				"Please confirm:",
				"Are you really sure?",
				"I am", "Not quite",
				new ConfirmDialog.Listener() {
					public void onClose(ConfirmDialog dialog) {
						if (! dialog.isConfirmed()) {
							return;
						}
						SimulationsListView parent = SimulationsListView.this; 
						try {
							parent.app.getSimulationManager().deleteFinishedSimulations();
							parent.enableButtons(false);
							parent.reloadSimulationsList();
						} catch (GridopException ex) {
							getLogger().log(Level.SEVERE, "The finished simulation items could not be removed from the table.", ex);
						}
					}
				});
		return;
	}
	
	
	/**
	 * Handle the click events related to the action buttons.
	 */
	public void buttonClick(ClickEvent event) {
		
		Button source = event.getButton();
		if (source == this.buttonDelete) {
			this.doDeleteSimulationAction();
			
		} else if (source == this.buttonDeleteFinished) {
			this.doDeleteFinishedSimulationsAction();
		}
		
		return;
	}
	
	
	/**
	 * Handle changed selected items on the table.
	 */
	public void valueChange(ValueChangeEvent event) {
		boolean enabled = (event.getProperty().getValue() != null)? true : false;
		this.enableButtons(enabled);
		
		return;
	}
	
	
	/**
	 * Return the list of actions of the table's context-menu.
	 */
	public Action[] getActions(Object target, Object sender) {
		return TABLE_ITEM_ACTIONS;
	}
	
	
	/**
	 * Handle click events on the table's context-menu.
	 */
	public void handleAction(Action action, Object sender, Object target) {
		
		this.simulationsTable.select(target);
		
		if ((action == ACTION_DELETE) && (target != null)) {
			this.doDeleteSimulationAction();
			
		} else if ((action == ACTION_DELETE_FINISHED) && (target != null)) {
			this.doDeleteFinishedSimulationsAction();
		}
		
		return;
	}
	
	
	/**
	 * Enable or disable buttons depending on a row is selected.
	 */
	private void enableButtons(boolean enabled) {
		this.buttonDelete.setEnabled(enabled);
		return;
	}
	
	
	/**
	 * Logger.
	 */
	private static final Logger getLogger() {
		return Logger.getLogger(SimulationsListView.class.getName());
	}
}
