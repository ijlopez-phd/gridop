package es.siani.gridop.gui;

import java.util.logging.Logger;

import com.vaadin.data.Item;
import com.vaadin.data.util.BeanItem;
import com.vaadin.data.validator.StringLengthValidator;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.DefaultFieldFactory;
import com.vaadin.ui.Field;
import com.vaadin.ui.Form;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.Component;
import com.vaadin.ui.Window;

import es.siani.gridop.GridOperatorApplication;
import es.siani.gridop.gui.ClientsGroupView;
import es.siani.gridop.model.ClientsGroup;

@SuppressWarnings("serial")
public class ClientsGroupView extends Window implements ClickListener, Window.CloseListener {
	
	/** Visible properties of the form */
	public static final String visibleProperties[] = {"code", "name", "description"};
	
	// Actions buttons
	private final Button buttonSave = new Button("Save", (ClickListener)this);
	private final Button buttonCancel = new Button("Cancel", (ClickListener)this);
	private final Button buttonOk = new Button("Ok", (ClickListener)this);
	
	/** Main application object */
	private final GridOperatorApplication app;
	
	/** Parent view: Clients Groups list view */
	private final ClientsGroupsListView parentView;
	
	/** Form with the data fields of the ClientsGroup */
	private final Form form;
	
	
	/**
	 * Constructor.
	 */
	public ClientsGroupView(GridOperatorApplication app, ClientsGroupsListView parentView, final DisplayModeEnum displayMode, Item clientGroupItem) {
		
		super("Clients Group information");
		
		this.app = app;
		this.parentView = parentView;
		
		// Size and position of the popup
		this.center();
		this.getContent().setSizeUndefined();
		this.setResizable(false);
		
		// Build the form and the input fields.
		this.form = new Form();
		this.form.setSizeUndefined();
		this.form.getLayout().setSizeUndefined();
		this.form.setWriteThrough(true);
		final boolean readOnlyMode = (displayMode == DisplayModeEnum.READ)? true : false;
		
		
		// Se the field factory.
		this.form.setFormFieldFactory(new DefaultFieldFactory(){
			@Override
			public Field createField(Item item, Object propertyId, Component uiContext) {
				
				String propName = propertyId.toString();
				
				if (propName.equalsIgnoreCase("CODE")) {
					TextField field = new TextField();
					field.setRequired(true);
					field.setRequiredError("The Code is missing.");
					field.setCaption("Code");
					field.setDescription("Short name that identifies the group of clients.");
					field.setNullRepresentation("");
					field.setReadOnly(readOnlyMode);
					return field;
					
				} else if (propName.equalsIgnoreCase("NAME")) {
					TextField field = new TextField();
					field.setRequired(true);
					field.setRequiredError("The Name is missing.");
					field.setCaption("Name");
					field.setDescription("Name of the group of clients.");
					field.setNullRepresentation("");
					field.addValidator(new StringLengthValidator(
							"The length of the field must be less than or equal to 256 characters.", 1, 256, false));
					field.setReadOnly(readOnlyMode);
					return field;
					
				} else if (propName.equalsIgnoreCase("DESCRIPTION")) {
					TextArea field = new TextArea();
					field.setRequired(false);
					field.setRequiredError("The Description is missing.");
					field.setCaption("Description");
					field.setDescription("Brief description of the group of clients.");
					field.setNullRepresentation("");
					field.setRows(5);
					field.addValidator(new StringLengthValidator(
							"The length of the field must be less than or equal to 1024 characters.", 1, 1024, false));
					field.setReadOnly(readOnlyMode);
					return field;					
				}
				
				return null;
			}
		});
		
		this.form.setItemDataSource(clientGroupItem);
		this.form.setVisibleItemProperties(visibleProperties);
		
		// Buttons
		HorizontalLayout footerLayout = new HorizontalLayout();
		footerLayout.addStyleName("ign-buttonsbar");
		footerLayout.setWidth("100%");
		Panel buttonsPanel = new Panel();
		buttonsPanel.setSizeUndefined();
		buttonsPanel.addStyleName("ign-buttonswrap");
		HorizontalLayout buttonsLayout = new HorizontalLayout();
		buttonsLayout.setSpacing(true);
		buttonsLayout.setSizeUndefined();
		buttonsPanel.setContent(buttonsLayout);
		if ((displayMode == DisplayModeEnum.NEW) || (displayMode == DisplayModeEnum.EDIT)) {
			buttonsPanel.addComponent(this.buttonCancel);
			buttonsPanel.addComponent(this.buttonSave);
		} else if (displayMode == DisplayModeEnum.READ){
			buttonsPanel.addComponent(this.buttonOk);
		}
		
		footerLayout.addComponent(buttonsPanel);
		footerLayout.setComponentAlignment(buttonsPanel, Alignment.MIDDLE_RIGHT);
		this.form.setFooter(footerLayout);
		
		
		this.getContent().addComponent(form);
		
		return;
	}
	
	
	/**
	 * Handle the click events related to the buttons of the footer.
	 */
	public void buttonClick(ClickEvent event) {
		
		final Button source = event.getButton();
		
		if (source == this.buttonSave) {
			try {
				// Commit the changes from the Form to the Item.
				this.form.commit();
				// Commit the changes to the database.
				@SuppressWarnings("unchecked")
				BeanItem<ClientsGroup> item = (BeanItem<ClientsGroup>)this.form.getItemDataSource();
				this.app.getClientManager().createClientsGroup(item.getBean());
				this.parentView.reloadList();
			} catch (Exception ex) {
				getLogger().severe("The Clients Group data could not be stored in the database.");
			}
			this.close();
			
		} else if (source == this.buttonCancel) {
			// Discard changes on the form's data.
			this.form.discard();
			this.close();
			
		} else if (source == this.buttonOk) {
			this.close();
		}
		
		return;
	}
	
	
	/**
	 * Handle the close window event.
	 */
	public void windowClose(CloseEvent e) {
		this.form.discard();
	}	
	
	
	/**
	 * Logger.
	 */
	private static final Logger getLogger() {
		return Logger.getLogger(ClientsGroupView.class.getName());
	}
}
