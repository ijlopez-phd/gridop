package es.siani.gridop.gui;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.vaadin.data.Item;
import com.vaadin.data.util.BeanItem;
import com.vaadin.data.validator.StringLengthValidator;
import com.vaadin.event.FieldEvents.TextChangeEvent;
import com.vaadin.event.FieldEvents.TextChangeListener;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Component;
import com.vaadin.ui.DefaultFieldFactory;
import com.vaadin.ui.Field;
import com.vaadin.ui.Form;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextArea;
import es.siani.gridop.GridOperatorApplication;
import es.siani.gridop.GridopException;
import es.siani.gridop.model.GridOperator;

@SuppressWarnings("serial")
public class GridOperatorView extends Form implements ClickListener, TextChangeListener {
	
	/** Visible properties of the form */
	public static final String visibleProperties[] = {"description"};
	
	/** Actions buttons of the form */
	private final Button buttonSave = new Button("Save", (ClickListener)this);
	private final Button buttonReset = new Button("Discard", (ClickListener)this);
	
	/** Reference to the main application object */
	private final GridOperatorApplication app;
	
	
	/**
	 * Constructor.
	 */
	public GridOperatorView(final GridOperatorApplication app) {
		
		this.app= app;
		
		this.setWriteThrough(false);
		
		// Footer buttons (save and discard)
		HorizontalLayout footerLayout = new HorizontalLayout();
		footerLayout.setSpacing(true);
		footerLayout.addComponent(this.buttonSave);
		footerLayout.addComponent(this.buttonReset);
		this.setFooter(footerLayout);
		
		
		// Set the field factory
		this.setFormFieldFactory(new DefaultFieldFactory() {
			@Override
			public Field createField(Item item, Object propertyId, Component uiContext) {
				
				String propName = propertyId.toString();
					
				if (propName.equalsIgnoreCase("DESCRIPTION")) {
					TextArea field = new TextArea();
					field.setRequired(false);
					field.setCaption("Description");
					field.setDescription("Comments about the Grid Operator application.");
					field.addValidator(new StringLengthValidator(
							"The length of the field must be less than 1024 characters.", 0, 1024, false));					
					field.setNullRepresentation("");
					field.setRows(5);
					field.addListener((TextChangeListener)GridOperatorView.this);
					field.setImmediate(true);
					
					return field;
					
				}
				
				return null;
			}
		});
		
		// Attach the data source to the form.
		try {
			GridOperator gridOperator = this.app.getGridOperatorManager().getGridOperator();
			if (gridOperator == null) {
				this.setItemDataSource(new BeanItem<GridOperator>(new GridOperator()));
			} else {
				this.setItemDataSource(new BeanItem<GridOperator>(gridOperator));
			}
		} catch (GridopException ex) {
			getLogger().log(Level.SEVERE, "Couldn't get the Grid Operator info from the datastore.");
		}
		
		// Set the visible properties of the data source.
		this.setVisibleItemProperties(GridOperatorView.visibleProperties);
		
		// Initial state of buttons.
		this.enableButtons(false);
		
		return;
	}
	
	
	/**
	 * Enable and disable "Save" and "Discard" buttons.
	 */
	private void enableButtons(boolean enabled) {
		this.buttonSave.setEnabled(enabled);
		this.buttonReset.setEnabled(enabled);
		
		return;
	}
	
	
	/**
	 * Updates the data source with changes since the last commit.
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void commit() {
		// Commit the data entered in the form to the actual item.
		super.commit();
		
		// Commit the changes to the database.
		try {
			BeanItem<GridOperator> item = (BeanItem<GridOperator>)this.getItemDataSource();
			this.app.getGridOperatorManager().saveGridOperator((GridOperator)item.getBean());
		} catch (GridopException ex) {
			getLogger().log(Level.SEVERE, "Grid Operator data couldn't be stored in the database.");
		}
		
		return;
	}
	
	
	/**
	 * Discard changes since the last commit.
	 */
	@Override
	public void discard() {
		// Discard changes on the actual item.
		super.discard();
	}
	
	
	/**
	 * Handle the click events related to the "save" and "reset" buttons.
	 */
	public void buttonClick(ClickEvent event) {
		
		final Button source = event.getButton();
		
		if (source == this.buttonSave) {
			// Store the form's data
			this.commit();
			this.enableButtons(false);
			
		} else if (source == this.buttonReset) {
			// Discard changes on the form's data
			this.discard();
			this.enableButtons(false);
		}
		
		return;
	}
	
	
	/**
	 * Handle the events related to the changes on the text fields.
	 */
	public void textChange(TextChangeEvent event) {
		this.enableButtons(true);
		
		return;
	}
	
	
	/**
	 * Logger. 
	 */
    private static final Logger getLogger() {
        return Logger.getLogger(GridOperatorView.class.getName());
    }	
}
