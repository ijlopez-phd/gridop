package es.siani.gridop.gui;

import com.vaadin.data.Item;
import com.vaadin.data.validator.StringLengthValidator;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.DefaultFieldFactory;
import com.vaadin.ui.Field;
import com.vaadin.ui.Form;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextField;
import com.vaadin.ui.Window;
import com.vaadin.ui.Window.CloseListener;

@SuppressWarnings("serial")
public class ClientPopup extends Window implements ClickListener, CloseListener {
	
	/** Visible properties of the form */
	private static final String visibleProperties[] = {"code", "xmppAddress", "remoteXmppAddress", "aspem.code"};
	
	// Actions buttons
	private final Button buttonOk = new Button("Ok", (ClickListener)this);
	
	/** Form with the data fields of the client */
	private final Form form;	
	
	
	/**
	 * Constructor.
	 */
	public ClientPopup(Item itemDataSource) {
		super("Client Information");
		
		// Size and position of the popup
		this.center();
		this.getContent().setSizeUndefined();
		this.setResizable(false);

		// Build the form within the input fields
		this.form = new Form();
		this.form.setSizeUndefined();
		this.form.getLayout().setSizeUndefined();
		this.form.setWriteThrough(true);
		
		// Set the field factory of the form
		this.form.setFormFieldFactory(new DefaultFieldFactory() {
			@Override
			public Field createField(Item item, Object propertyId, Component uiContext) {
				
				String propName = propertyId.toString();
				
				if (propName.equalsIgnoreCase("CODE")) {
					TextField field = new TextField();
					field.setRequired(true);
					field.setCaption("Code");
					field.setDescription("Unique token that identifies the Client");
					field.setNullRepresentation("");
					field.addValidator(new StringLengthValidator(
							"The length of the field must be less than or equal to 256 characters.", 1, 256, false));
					field.setReadOnly(true);
					return field;
					
				} else if (propName.equalsIgnoreCase("XMPPADDRESS")) {
					TextField field = new TextField();
					field.setRequired(true);
					field.setCaption("Broker XMPP");
					field.setDescription("XMPP address of the Broker Agent that represents the client.");
					field.setNullRepresentation("");
					field.addValidator(new StringLengthValidator(
							"The length of the field must be less than or equal to 512 characters.", 1, 512, false));
					field.setReadOnly(true);
					return field;
					
				} else if (propName.equalsIgnoreCase("REMOTEXMPPADDRESS")) {
					TextField field = new TextField();
					field.setRequired(true);
					field.setCaption("AS-Box XMPP");
					field.setDescription("XMPP address of the AS-Box of the client.");
					field.setNullRepresentation("");
					field.addValidator(new StringLengthValidator(
							"The length of the field must be less than or equal to 512 characters.", 1, 512, false));
					field.setReadOnly(true);
					return field;					
					
				} else if (propName.equalsIgnoreCase("ASPEM.CODE")) {
					TextField field = new TextField();
					field.setRequired(true);
					field.setCaption("ASPEM");
					field.setDescription("Code of the ASPEM to which the client belongs to.");
					field.setNullRepresentation("");
					field.addValidator(new StringLengthValidator(
							"The length of the field must be less than or equal to 128 characters.", 1, 128, false));
					field.setReadOnly(true);
					return field;					
				}
				
				return null;
			}
		});
		
		this.form.setItemDataSource(itemDataSource);
		this.form.setVisibleItemProperties(ClientPopup.visibleProperties);		

		// Buttons
		HorizontalLayout footerLayout = new HorizontalLayout();
		footerLayout.addStyleName("ign-buttonsbar");
		footerLayout.setWidth("100%");
		Panel buttonsPanel = new Panel();
		buttonsPanel.setSizeUndefined();
		buttonsPanel.addStyleName("ign-buttonswrap");
		HorizontalLayout buttonsLayout = new HorizontalLayout();
		buttonsLayout.setSpacing(true);
		buttonsLayout.setSizeUndefined();
		buttonsPanel.setContent(buttonsLayout);
		buttonsPanel.addComponent(this.buttonOk);
		
		footerLayout.addComponent(buttonsPanel);
		footerLayout.setComponentAlignment(buttonsPanel, Alignment.MIDDLE_RIGHT);
		this.form.setFooter(footerLayout);
		
		this.getContent().addComponent(form);
		
		return;			
	}

	
	/**
	 * Handle the close window event.
	 */
	public void windowClose(CloseEvent e) {
		this.form.discard();
	}

	
	/**
	 * Handle the click events related to the buttons.
	 */
	public void buttonClick(ClickEvent event) {
		final Button source = event.getButton();
		if (source == this.buttonOk) {
			// Just close the window
			this.close();
		}
		
		return;
	}
}
