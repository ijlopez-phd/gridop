package es.siani.gridop.gui;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;

import org.vaadin.addon.customfield.CustomField;

import com.vaadin.data.Property;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Upload.FailedEvent;
import com.vaadin.ui.Upload.FinishedEvent;
import com.vaadin.ui.Upload.StartedEvent;
import com.vaadin.ui.Upload.SucceededEvent;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.Upload;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

import es.siani.gridop.GridOperatorApplication;
import es.siani.gridop.model.GridopFile;
import es.siani.gridop.parser.GridopFileParser;
import es.siani.gridop.parser.GridopFileParserException;

@SuppressWarnings("serial")
public class UploaderField extends CustomField implements Button.ClickListener, UploadPopupListener, Property.ValueChangeListener {
	
	/** Reference to the main application object */
	private final GridOperatorApplication app;
	
	/** Reference to the parser to validate the uploaded file. */
	private GridopFileParser fieldParser;
	
	/** Stores the name of the field. */
	protected String caption = "";
	
	protected Label fileLabel = new Label("");
	protected Button uploadButton = new Button("Upload", (ClickListener)this);
	
	
	/**
	 * Constructor.
	 */
	public UploaderField(GridOperatorApplication app) {
		this(app, null);
	}
	
	
	/**
	 * Constructor.
	 */
	public UploaderField(GridOperatorApplication app, GridopFileParser fieldParser)  {
		
		this.app = app;
		this.fieldParser = fieldParser;
		
		// Main layout of the field
		HorizontalLayout layout = new HorizontalLayout();
		layout.setSizeFull();
		layout.setSpacing(true);
		
		// File label
		layout.addComponent(this.fileLabel);
		layout.setComponentAlignment(fileLabel, Alignment.MIDDLE_LEFT);
		
		// Buttons layout
		HorizontalLayout buttonsLayout = new HorizontalLayout();
		buttonsLayout.setSpacing(true);
		buttonsLayout.setSizeUndefined();
		
		// - Button for uploading a Program.
		buttonsLayout.addComponent(this.uploadButton);
		layout.addComponent(buttonsLayout);
		
		this.addListener((ValueChangeListener)this);
		

		this.setCompositionRoot(layout);
		
		return;
	}
	
	
	/**
	 * Sets the Caption.
	 */
	public void setCaption(String caption) {
		this.caption = caption;
		requestRepaint();
		return;
	}
	
	
	/**
	 * Gets the Caption.
	 * @see com.vaadin.ui.Field#getCaption()
	 */
	@Override
	public String getCaption() {
		return this.caption;
	}
	
	
	/**
	 * @see com.vaadin.data.Property#getType() 
	 */
	@Override
	public Class<?> getType() {
		return GridopFile.class;
	}
	

	/**
	 * Handle the Click buttons event.
	 */
	@Override
	public void buttonClick(ClickEvent event) {
		
		final Button source = event.getButton();
		
		if (source == this.uploadButton) {
			UploadPopup popup = new UploadPopup(this);
			this.app.getMainWindow().addWindow(popup);
		}
		
		return;
	}
	
	
	/**
	 * Called by the UploadPopup whenever the file is successfully uploaded.
	 */
	@Override
	public void uploadSucceeded(GridopFile fileContent) {
		setValue(fileContent);
		return;
	}
	
	
	/**
	 * Listener related to the change of the value of the field.
	 */
	@Override
	public void valueChange(com.vaadin.data.Property.ValueChangeEvent event) {
		this.fileLabel.setValue(
				(getValue() != null)? ((GridopFile)getValue()).getName() : null);
		return;		
	}
	
	
	/**
	 * Shows a popup that allows the user to upload a file.
	 */
	private class UploadPopup extends Window implements ClickListener, Window.CloseListener {
		
		private final Button buttonCancel = new Button("Cancel", (ClickListener)this);
		private Label errorLabel = new Label("", Label.CONTENT_XHTML);
		private Upload upload;
		private ByteArrayOutputStream ostream;
		
		
		/**
		 * Constructor.
		 */
		public UploadPopup(final UploadPopupListener parent) {
			super("Upload file");
			
			// Size and position of the popup
			this.center();
			this.setResizable(false);
			this.setSizeUndefined();
			
			// Layout of the popup
			VerticalLayout layout = new VerticalLayout();
			layout.setSpacing(true);
			layout.setMargin(true);
			layout.setSizeUndefined();
			
			// - Upload control
			this.upload = new Upload();
			this.upload.setButtonCaption("Upload");
			layout.addComponent(this.upload);
			
			// - Error label
			this.errorLabel.addStyleName("error");
			layout.addComponent(this.errorLabel);
			layout.setComponentAlignment(this.errorLabel, Alignment.TOP_CENTER);
			
			// - Buttons
			HorizontalLayout footerLayout = new HorizontalLayout();
			footerLayout.addStyleName("ign-buttonsbar");
			footerLayout.setWidth("100%");
			Panel buttonsPanel = new Panel();
			buttonsPanel.setSizeUndefined();
			buttonsPanel.addStyleName("ign-buttonswrap");
			HorizontalLayout buttonsLayout = new HorizontalLayout();
			buttonsLayout.setSpacing(true);
			buttonsLayout.setSizeUndefined();
			buttonsPanel.setContent(buttonsLayout);
			buttonsPanel.addComponent(this.buttonCancel);
			footerLayout.addComponent(buttonsPanel);
			footerLayout.setComponentAlignment(buttonsPanel, Alignment.MIDDLE_RIGHT);
			layout.addComponent(footerLayout);
			this.enableButtons(true);
			
			// Content
			this.setContent(layout);
			
			// Listener of the upload
			this.upload.setReceiver(new Upload.Receiver() {
				@Override
				public OutputStream receiveUpload(String filename, String mimeType) {
					ostream = new ByteArrayOutputStream();
					return ostream;
				}
			});
			
			this.upload.addListener(new Upload.StartedListener() {
				@Override
				public void uploadStarted(StartedEvent event) {
					setClosable(false);
					enableButtons(false);
					return;
				}
			});
			
			this.upload.addListener(new Upload.FinishedListener() {
				@Override
				public void uploadFinished(FinishedEvent event) {
					setClosable(true);
					enableButtons(true);
					return;
				}
			});
			
			this.upload.addListener(new Upload.SucceededListener() {
				@Override
				public void uploadSucceeded(SucceededEvent event) {
					
					// Get the content of the file and close the stream.
					String content = ostream.toString();
					try {
						ostream.close();
					} catch (Exception ex) {
						// Do nothing
					} finally {
						ostream = null;
					}				
					
					if (content.length() == 0) {
						errorLabel.setCaption("The file seems to be empty. Try again.");
					} else if (fieldParser != null) {
						try {
							GridopFile fileDescriptor = fieldParser.validate(content);
							parent.uploadSucceeded(new GridopFile(fileDescriptor.getName(), fileDescriptor.getContent()));
							close();							
						} catch (GridopFileParserException ex) {
							errorLabel.setCaption("The content of the file is not valid. Try again.");
						}
					} else {
						parent.uploadSucceeded(new GridopFile(event.getFilename(), content));
						close();						
					}
					
					return;
				}
			});
			
			this.upload.addListener(new Upload.FailedListener() {
				@Override
				public void uploadFailed(FailedEvent event) {
					errorLabel.setCaption("Error occurred during uploading the file. Try again.");
					return;
				}
			});			
			
			return;
		}
		
		/**
		 * Enable or disable buttons.
		 */
		private void enableButtons(boolean enabled) {
			this.buttonCancel.setEnabled(enabled);
			return;
		}		
		

		/**
		 * Handles the Close window event.
		 */
		@Override
		public void windowClose(CloseEvent e) {
			discard();
			close();
			return;
		}


		/**
		 * Handles the Click button event.
		 */
		@Override
		public void buttonClick(ClickEvent event) {
			
			final Button source = event.getButton();
			
			if (source == this.buttonCancel) {
				close();
			}
			
			return;
		}
	}
}

interface UploadPopupListener {
	public void uploadSucceeded(GridopFile ud);
}