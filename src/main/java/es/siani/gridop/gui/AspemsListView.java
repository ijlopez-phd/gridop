package es.siani.gridop.gui;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.vaadin.data.util.BeanItem;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.event.Action;
import com.vaadin.terminal.ClassResource;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.ui.Embedded;
import com.vaadin.ui.Layout.MarginInfo;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Panel;
import com.vaadin.ui.Table;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.Runo;
import es.siani.gridop.GridOperatorApplication;
import es.siani.gridop.GridopException;
import es.siani.gridop.model.Aspem;
import static es.siani.gridop.GridopGlobals.*;


@SuppressWarnings("serial")
public class AspemsListView
		extends Panel
		implements ClickListener, Table.ValueChangeListener, Action.Handler {
	
	/** Natural property order of the columns */
	private static final Object[] NATURAL_COL_ORDER = new Object[] {
		"code", "name", "statusicon"
	};
	
	/** Human readable captions for properties (in same ordes as NATURAL_COL_ORDER) */
	private static final String[] COL_CAPTIONS = new String[] {
		"Code", "Name", "Status"
	};
	
	/** Button for viewing an item of the table */
	private final Button buttonView = new Button("View", (ClickListener)this);
	
	/** Context-menu action for viewing an item of the table */
	private static final Action ACTION_VIEW = new Action("View");
	private static final Action TABLE_ITEM_ACTIONS[] = {ACTION_VIEW};
	
	/** Reference to the main application object */
	private final GridOperatorApplication app;
	
	/** Table that lists all the ASPEMs registered with the Grid Operator System */
	private final Table aspemsTable = new Table();
	
	
	/**
	 * Constructor.
	 */
	public AspemsListView(final GridOperatorApplication app) {
		super();
		
		this.app = app;
		
		// Populate the table's container with the registered ASPEMs
		BeanItemContainer<Aspem> container = new BeanItemContainer<Aspem>(Aspem.class);
		try {
			List<Aspem> aspems = this.app.getAspemManager().getAllAspems();
			container.addAll(aspems);
		} catch (GridopException ex) {
			getLogger().severe("The list of ASPEMs could not be retrieved from the database.");
		}
		
		// Main panel without borders
		this.addStyleName(Runo.PANEL_LIGHT);
		
		// Layout of the view
		VerticalLayout layout = new VerticalLayout();
		layout.setSizeFull();
		
		// Generated columns
		this.aspemsTable.addContainerProperty("statusicon", Embedded.class, null, "Status", null, Table.ALIGN_CENTER);
		this.aspemsTable.addGeneratedColumn("statusicon", this.new StatusIconColumnGenerator());
		
		// Table
		this.aspemsTable.setSizeFull();
		this.aspemsTable.setContainerDataSource(container);
		this.aspemsTable.setVisibleColumns(NATURAL_COL_ORDER);
		this.aspemsTable.setColumnHeaders(COL_CAPTIONS);
		this.aspemsTable.setSelectable(true);
		this.aspemsTable.setImmediate(true);
		this.aspemsTable.setColumnReorderingAllowed(false);
		this.aspemsTable.setMultiSelect(false);
		this.aspemsTable.setPageLength(TABLE_MAX_VISIBLE_ITEMS);
		this.aspemsTable.addListener((Table.ValueChangeListener)this);
		this.aspemsTable.addActionHandler(this);
		layout.addComponent(this.aspemsTable);
		
		// Buttons for actions
		HorizontalLayout buttonsLayout = new HorizontalLayout();
		buttonsLayout.setMargin(new MarginInfo(true, false, true, false));
		buttonsLayout.setSpacing(true);
		buttonsLayout.addComponent(this.buttonView);
		layout.addComponent(buttonsLayout);
		
		// Some buttons are not enabled until a row is selected
		this.enableButtons(false);
		
		// Set the content of the panel
		this.setContent(layout);
		
		return;
	}
	
	
	/**
	 * View the detailed description of the ASPEM in a popup.
	 */
	private void doViewAspemAction() {
		
		Aspem aspem = (Aspem)this.aspemsTable.getValue();
		if (aspem != null) {
			AspemPopup popup = new AspemPopup(new BeanItem<Aspem>(aspem));
			this.app.getMainWindow().addWindow(popup);
		}
		
		return;
	}
	

	/**
	 * Handle events of the table's buttons.
	 */
	public void buttonClick(ClickEvent event) {
		Button source = event.getButton();
		
		if (source == this.buttonView) {
			this.doViewAspemAction();
		}
		
		return;
	}
	
	
	/**
	 * Handle click events on the table's context-menu.
	 */
	public void handleAction(Action action, Object sender, Object target) {
		this.aspemsTable.select(target);
		
		if ((action == ACTION_VIEW) && (target != null)) {
			this.doViewAspemAction();
		}
		
		return;
	}
	
	
	/**
	 * Handle changes of the selected items of the table.
	 */
	public void valueChange(ValueChangeEvent event) {
		boolean enabled = (event.getProperty().getValue() != null)? true : false;
		this.enableButtons(enabled);
		
		return;
	}
	
	
	/**
	 * Return the list of actions of the table's context-menu.
	 */
	public Action[] getActions(Object target, Object sender) {
		return TABLE_ITEM_ACTIONS;
	}
	
	
	/**
	 * Enable or disable buttons depending on a row is selected.
	 */
	private void enableButtons(boolean enabled) {
		this.buttonView.setEnabled(enabled);
		return;
	}
	
	
	/**
	 * Reload the ASPEMs listed in the table.
	 */
	@SuppressWarnings("unchecked")
	public void reloadAspemsList() {
		
		BeanItemContainer<Aspem> container = (BeanItemContainer<Aspem>)this.aspemsTable.getContainerDataSource();
		
		try {
			List<Aspem> aspems = this.app.getAspemManager().getAllAspems();
			container.removeAllItems();
			container.addAll(aspems);
		} catch (GridopException ex) {
			getLogger().log(Level.SEVERE, "Trying to get list of ASPEMs.");
		}
		
		return;
	}
	
	
	/**
	 * Logger.
	 */
	private static final Logger getLogger() {
		return Logger.getLogger(AspemsListView.class.getName());
	}
	
	
	/**
	 * Class for generating the icon of the Status column. 
	 */
	private class StatusIconColumnGenerator implements Table.ColumnGenerator {

		public Object generateCell(Table source, Object itemId, Object columnId) {
			Property prop = source.getItem(itemId).getItemProperty("status");
			
			Boolean status = (Boolean)prop.getValue();
			
			Embedded image;
			if ((status == null) || (!status)) {
				image = new Embedded(
						"Stopped",
						new ClassResource("images/stopped.png", AspemsListView.this.app));
			} else {
				image = new Embedded(
						"Active",
						new ClassResource("images/ok.png", AspemsListView.this.app));				
			}
			
			return image;
		}
	}
}
