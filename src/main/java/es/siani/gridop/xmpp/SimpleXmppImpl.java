package es.siani.gridop.xmpp;

import java.util.logging.Logger;

import javax.inject.Inject;

import org.jivesoftware.smack.AccountManager;
import org.jivesoftware.smack.Connection;
import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import es.siani.gridop.GridopException;
import es.siani.gridop.config.Configuration;
import es.siani.gridop.model.Client;
import static es.siani.gridop.config.Configuration.*;


public class SimpleXmppImpl implements SimpleXmpp {
	
	protected static final int ERR_ACCOUNT_ALREADY_EXISTS = 409;
	
	protected Connection connection;
	protected String serverUrl;
	protected int serverPort;
	protected String userLogin;
	protected String userPass;
	protected String userResource;
	
	
	/**
	 * Constructor.
	 */
	@Inject
	public SimpleXmppImpl(Configuration appConfig) {
		this.serverUrl = appConfig.get(PROP_XMPP_URL);
		this.serverPort = Integer.parseInt(appConfig.get(PROP_XMPP_PORT));
		this.userLogin = appConfig.get(PROP_XMPP_LOGIN);
		this.userPass = appConfig.get(PROP_XMPP_PASS);
		this.userResource = appConfig.get(PROP_XMPP_RESOURCE);
		return;
	}
	
	
	/**
	 * @see es.siani.gridop.xmpp.SimpleXmpp#connect()
	 */
	public void connect() throws GridopException {
		
		if ((this.connection != null) && (this.connection.isConnected())) {
			return;
		}
		
		ConnectionConfiguration xmppConfig = new ConnectionConfiguration(this.serverUrl, this.serverPort);
		this.connection = new XMPPConnection(xmppConfig);
		
		try {
			this.connection.connect();
			this.connection.login(this.userLogin, this.userPass, this.userResource);
		} catch (XMPPException ex) {
			getLogger().severe("Could not establish connection to the XMPP server.");
			throw new GridopException(ex);
		}
		
		return;
	}
	
	
	/**
	 * @see es.siani.gridop.xmpp.SimpleXmpp#disconnect()
	 */
	public void disconnect() {
		
		if ((this.connection == null) || (!this.connection.isConnected())) {
			return;
		}
		
		this.connection.disconnect();
		
		return;
	}
	
	
	/**
	 * @see es.siani.gridop.xmpp.SimpleXmpp#register(Client)
	 */
	public void register(Client client) throws GridopException {
		
		this.connect();
		
		AccountManager accountManager = this.connection.getAccountManager();
		
		// Create the account for the Broker Agent.
		try {
			String address = this.createBrokerAccountAddress(client);
			String username = this.getUsernameFromAccountAddress(address);
			client.setXmppAddress(address);			
			accountManager.createAccount(
					username,
					this.generatePassword(username));
		} catch (XMPPException ex) {
			if (ex.getXMPPError().getCode() == ERR_ACCOUNT_ALREADY_EXISTS) {
				getLogger().warning("The account already exists in the XMPP server.");
			} else {
				getLogger().severe("Could not create the local client account in the XMPP server.");
				throw new GridopException(ex);
			}
		}
		
		// Create the account for the agent of the ASBox.
		try {
			String address = this.createAsBoxAccountAddress(client);
			String username = this.getUsernameFromAccountAddress(address);
			client.setRemoteXmppAddress(address);			
			accountManager.createAccount(
					username,
					this.generatePassword(username));
		} catch (XMPPException ex) {
			if (ex.getXMPPError().getCode() == ERR_ACCOUNT_ALREADY_EXISTS) {
				getLogger().warning("The account already exists in the XMPP server.");
			} else {
				getLogger().severe("Could not create the local client account in the XMPP server.");
				throw new GridopException(ex);
			}
		}
		
		return;
	}
	
	
	/**
	 * @see es.siani.gridop.xmpp.SimpleXmpp#deregister(Client)
	 */
	public void deregister(Client client) throws GridopException {
		
		ConnectionConfiguration xmppConfig = new ConnectionConfiguration(this.serverUrl, this.serverPort);
		Connection conn = null;

		// Try to delete the account corresponding to the Broker Agent.
		try {
			conn = new XMPPConnection(xmppConfig);
			conn.connect();
			String username = this.getUsernameFromAccountAddress(client.getXmppAddress());
			conn.login(username, this.generatePassword(username));
			conn.getAccountManager().deleteAccount();
		} catch (Exception ex) {
			getLogger().warning("The local XMPP account of the client '" + client.getCode() + "' could not be deleted.");
		} finally {
			if (conn != null) {
				conn.disconnect();
				conn = null;
			}
		}
		
		// Try to delete the account corresponding to the ASBox.
		try {
			conn = new XMPPConnection(xmppConfig);
			conn.connect();
			String username = this.getUsernameFromAccountAddress(client.getRemoteXmppAddress());
			conn.login(username, this.generatePassword(username));
			conn.getAccountManager().deleteAccount();
		} catch (Exception ex) {
			getLogger().warning("The local XMPP account of the client '" + client.getCode() + "' could not be deleted.");
		} finally {
			if (conn != null) {
				conn.disconnect();
				conn = null;
			}
		}
		
		return;
	}
	
	
	/**
	 * @see es.siani.gridop.xmpp.SimpleXmpp#createBrokerAccountAddress(Client)
	 */
	public String createBrokerAccountAddress(Client client) {
		return "broker-" + client.getCode() + "-" + client.getAspem().getCode() + "@" + this.serverUrl;
	}
	
	
	/**
	 * @see es.siani.gridop.xmpp.SimpleXmpp#createAsBoxAccountAddress(Client)
	 */	
	public String createAsBoxAccountAddress(Client client) {
		return "asbox-" + client.getCode() + "-" + client.getAspem().getCode() + "@" + this.serverUrl;
	}
	
	
	/**
	 * @see es.siani.gridop.xmpp.SimpleXmpp#getUsernameFromAccountAddress(Client)
	 */
	public String getUsernameFromAccountAddress(String accountAddress) {
		
		int idx = accountAddress.indexOf("@" + this.serverUrl);
		if (idx == -1) {
			return null;
		}
		
		return accountAddress.substring(0, idx);
	}
	
	
	/**
	 * Generate a password from the account name.
	 * For the moment, the parameter is the password.
	 */
	private String generatePassword(String phrase) {
		return phrase;
	}
	
	
	/**
	 * Logger.
	 */
    private static final Logger getLogger() {
        return Logger.getLogger(SimpleXmppImpl.class.getName());
    }
}
