package es.siani.gridop.xmpp;

import es.siani.gridop.GridopException;
import es.siani.gridop.model.Client;

public interface SimpleXmpp {

	/**
	 * Connect to the XMPP server.
	 */
	public void connect() throws GridopException;
	

	/**
	 * Disconnect from the XMPP server.
	 */
	public void disconnect();
	
	
	/**
	 * Register a Client entity with the XMPP server.
	 * It also creates the account name of both the Broker agent and the ASBox.
	 *   This information is assigned to the Client parameter.
	 */
	public void register(Client client) throws GridopException;
	
	
	/**
	 * Deregister a Client entity from the XMPP server.
	 */
	public void deregister(Client client) throws GridopException;
	
	
	/**
	 * Create the XMPP account name for a broker agent of the ASPEM.
	 */
	public String createBrokerAccountAddress(Client client);
	
	
	/**
	 * Create the XMPP account name for an ASBox agent.
	 */
	public String createAsBoxAccountAddress(Client client);
	
	
	/**
	 * Create the XMPP account name for the agent instantiated in the ASPEM.
	 */
	public String getUsernameFromAccountAddress(String accountAddress);
}