package es.siani.gridop.services;

import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import es.siani.gridop.GridopException;
import es.siani.gridop.action.AspemManager;
import static es.siani.gridop.injection.InjectionUtils.*;

@Path("/aspem")
public class AspemResource {
	
	
//	/**
//	 * Register or update an ASPEM.
//	 */
//	@POST
//	@Consumes("application/x-www-form-urlencoded")
//	@Produces("text/plain")
//	public Response updateAspem(
//			@FormParam("globalId") Integer globalId,
//			@FormParam("code") String code,
//			@FormParam("name") String name,
//			@FormParam("url") String url) {
//		
//		Aspem aspem = new Aspem(globalId, code, name, url);
//		Integer aspemId;
//		try {
//			aspemId = getAspemManager().createUpdateAspem(aspem);
//		} catch (GridopException ex) {
//			return ServicesHelper.clientResponse(Response.Status.INTERNAL_SERVER_ERROR);
//		}
//		
//		return Response.status(Status.CREATED).entity(aspemId.toString()).build();
//	}
	
	
	/**
	 * Set the status of the ASPEM.
	 */
	@Path("{id}/status")
	@PUT
	public Response updateStatusAspem(
			@PathParam("id") int id,
			@QueryParam("status") boolean status) {
		
		try {
			getAspemManager().updateAspemStatus(id, status);
		} catch (GridopException ex) {
			return ServicesHelper.clientResponse(Response.Status.INTERNAL_SERVER_ERROR);
		}
		
		return Response.ok().build();
	}
	
	
	/**
	 * Operations related to the Clients associated to an ASPEM.
	 */
	@Path("{idAspem}/client")
	public ClientResource getClientResource() {
		return new ClientResource();
	}
	
	
	/**
	 * Build an AspemManager.
	 */
	private AspemManager getAspemManager() {
		return injector().getInstance(AspemManager.class);		
	}
	
	
//	/**
//	 * Build a SimulationManager.
//	 */
//	private SimulationManager getSimulationManager() {
//		return injector().getInstance(SimulationManager.class);
//	}
}
