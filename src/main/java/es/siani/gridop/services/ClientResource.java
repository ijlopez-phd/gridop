package es.siani.gridop.services;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import es.siani.gridop.GridopException;
import es.siani.gridop.action.AspemManager;
import es.siani.gridop.action.ClientManager;
import es.siani.gridop.model.Aspem;
import es.siani.gridop.model.Client;
import static es.siani.gridop.injection.InjectionUtils.*;

@Path("/client")
public class ClientResource {
	
	
	/**
	 * Service for deleting a client.
	 */
	@DELETE
	@Path("{id}")	
	public Response deleteClient(
			@PathParam("id") Integer id) {
		
		if (id == null) {
			return ServicesHelper.clientResponse(Response.Status.BAD_REQUEST);
		}
		
		try {
			getClientManager().deleteClient(id);
		} catch (GridopException ex) {
			return ServicesHelper.clientResponse(Response.Status.INTERNAL_SERVER_ERROR);
		}
		
		return Response.ok().build();
	}
	
	
	/**
	 * Service for registering a new client.
	 */
	@POST
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public Response newClient(
			@FormParam("aspemGlobalId") Integer aspemId,
			@FormParam("client") String xmlClient) {
		
		Client newClient;
		try {
			Client client = ServicesHelper.XMLToObject(Client.class, xmlClient);
			Aspem aspem = getAspemManager().getAspemById(aspemId);
			client.setAspem(aspem);
			newClient = getClientManager().createClient(client);
		} catch (GridopException argex) {
			return ServicesHelper.clientResponse(Response.Status.BAD_REQUEST);
		}

		
		return Response
				.status(Status.CREATED)
				.entity(ServicesHelper.ObjectToXML(newClient))
				.build();
	}	
	
	
	/**
	 * Service for updating a client.
	 */
	@PUT
	@Path("{id}")	
	public Response updateClient(
			@PathParam("id") Integer globalId,
			String xmlClient) {
		
		try {
			Client client = ServicesHelper.XMLToObject(Client.class, xmlClient);
			client.setId(globalId);
			getClientManager().updateClient(client);
		} catch (IllegalArgumentException argex) {
			return ServicesHelper.clientResponse(Response.Status.BAD_REQUEST);
		} catch(GridopException dataex) {
			return ServicesHelper.clientResponse(Response.Status.INTERNAL_SERVER_ERROR);
		}
		
		return Response.ok().build();
	}
	
	
	/**
	 * Build a Client Manager object.
	 */
	private ClientManager getClientManager() {
		return injector().getInstance(ClientManager.class);		
	}
	
	
	/**
	 * Build an ASPEM Manager object.
	 */
	private AspemManager getAspemManager() {
		return injector().getInstance(AspemManager.class);
	}	
}
