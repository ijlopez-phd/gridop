package es.siani.gridop.services;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import es.siani.gridop.GridopException;
import es.siani.gridop.action.ClientManager;
import es.siani.gridop.model.ClientsGroup;
import static es.siani.gridop.injection.InjectionUtils.*;

@Path("/clientsgroup")
public class ClientsGroupResource {
	
	@GET
	@Produces(MediaType.APPLICATION_XML)
	public Response getAllClientsGroups() {
		
		List<ClientsGroup> groups;
		try {
			 groups = getClientManager().getAllClientsGroups();
		} catch (GridopException e) {
			return ServicesHelper.clientResponse(Response.Status.INTERNAL_SERVER_ERROR);
		}
		
		return Response.ok().entity(
				new GenericEntity<List<ClientsGroup>>(groups){}).build();
	}
	
	
	/**
	 * Build a Client Manager object.
	 */
	private ClientManager getClientManager() {
		return injector().getInstance(ClientManager.class);		
	}	

}
