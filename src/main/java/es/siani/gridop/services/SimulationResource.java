package es.siani.gridop.services;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import es.siani.energyagents.EnergyAgentsGlobals;
import es.siani.gridop.action.SimulationManager;
import es.siani.gridop.model.Aspem;
import es.siani.gridop.model.PauseActionResult;
import es.siani.gridop.model.SimulationParams;
import static es.siani.gridop.injection.InjectionUtils.*;


@Path("/simulation")
public class SimulationResource {
	
	
	/**
	 * Initiates a new simulation. 
	 */
	@POST
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public Response start(
			@FormParam("scenarioCode") String scenarioCode,
			@FormParam("df") Integer nDirectories,
			@FormParam("of") Float overbookingFactor,
			@FormParam("usp") Boolean  useStartPrice,
			@FormParam("rnd") Boolean useRandom,
			@FormParam("cf") Integer constantF) {
		
		if (nDirectories == null) {
			nDirectories = EnergyAgentsGlobals.DF_DEFAULT_NUMBER;
		} else if (nDirectories < 1) {
			return ServicesHelper.clientResponse(Response.Status.BAD_REQUEST);
		}
		
		
		if (overbookingFactor == null) {
			overbookingFactor = EnergyAgentsGlobals.OVERBOOKING_FACTOR;
		} else if (overbookingFactor < 0) {
			return ServicesHelper.clientResponse(Response.Status.BAD_REQUEST);
		}
		
		
		if (useStartPrice == null) {
			useStartPrice = EnergyAgentsGlobals.USE_START_PRICE;
		}
		
		if (useRandom == null) {
			useRandom = EnergyAgentsGlobals.USE_RANDOM;
		}
		
		if (constantF == null) {
			constantF = EnergyAgentsGlobals.CONSTANT_F;
		}
		
		
		int idSimulation;
		try {
			idSimulation = getSimulationManager().simulationInitiated(
					scenarioCode,
					new SimulationParams(nDirectories, overbookingFactor, useStartPrice, useRandom, constantF));
		} catch (Exception ex) {
			return Response.serverError().build();
		}
		
		return Response.ok(new Integer(idSimulation).toString()).build();
	}
	
	
	/**
	 * Stops (finishes) and active simulation.
	 */
	@Path("{id}/finished")
	@PUT
	public Response finished(
			@PathParam("id") int idSimulation) {
		
		try {
			getSimulationManager().simulationFinished(idSimulation);
		} catch (Exception ex) {
			return Response.serverError().build();
		}
		
		return Response.ok().build();
	}
	
	
	/**
	 * Suspends and active simulation.
	 */
	@Path("{id}/paused")
	@PUT
	public Response suspended(
			@PathParam("id") int idSimulation,
			@QueryParam("rt") String realTime,
			@QueryParam("mt") String modelTime) {
		
		PauseActionResult pause;
		try {
			pause = getSimulationManager().simulationPaused(idSimulation, realTime, modelTime);
		} catch (Exception ex) {
			return Response.serverError().build();
		}
		
		return Response.ok(
				Long.toString(pause.getNextPause()) 
				+ ","
				+ Boolean.toString(pause.getDistributeActionTriggered())).build();
	}
	
	
	/**
	 * Create a new ASPEM for a specific simulation.
	 */
	@Path("{id}/aspem")
	@POST
	@Consumes(MediaType.APPLICATION_XML)
	public Response createAspem(@PathParam("id") int idSimulation, String content) {
		
		Aspem aspem;
		try {
			aspem = ServicesHelper.XMLToObject(Aspem.class, content);
		} catch (Exception ex) {
			return ServicesHelper.clientResponse(Response.Status.BAD_REQUEST);
		}
		
		try {
			SimulationManager sm = getSimulationManager();
			sm.runAspemServer(aspem);
		} catch (IllegalStateException ex) {
			return ServicesHelper.clientResponse(Response.Status.INTERNAL_SERVER_ERROR);
		}
		
		return Response.ok().build();
	}	
	
	
	/**
	 * Build a SimulationManager.
	 */
	private SimulationManager getSimulationManager() {
		return injector().getInstance(SimulationManager.class);		
	}
}
