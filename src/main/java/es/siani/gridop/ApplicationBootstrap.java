package es.siani.gridop;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import es.siani.gridop.data.DataProviderHelper;

public class ApplicationBootstrap implements ServletContextListener {
	
	/**
	 * Application shutdown event.
	 */
	public void contextDestroyed(ServletContextEvent event) {
		
		// Disconnect from the XMPP server
//		SimpleXmpp xmpp = injector().getInstance(SimpleXmpp.class);
//		xmpp.disconnect();
		
		return;
	}

	
	/**
	 * Application startup event.
	 */
	public void contextInitialized(ServletContextEvent event) {
		
		// Init the DataProvider.
		DataProviderHelper.initContext(event.getServletContext());
		
		// Connect to the XMPP server.
//		SimpleXmpp xmpp = injector().getInstance(SimpleXmpp.class);
//		try {
//			xmpp.connect();
//		} catch (GridopException ex) {
//			getLogger().severe("The application didn't initialize correctly. There XMPP module is not working.");
//		}
		
//		// Update the status of all the Aspems.
//		AspemManager aspemManager = injector().getInstance(AspemManager.class);
//		try {
//			aspemManager.updateAspemsStatus();
//		} catch (GridopException e) {
//			getLogger().severe("The current status of ASPEMs could not be sabed.");
//		}
		
		return;
	}
	
	
//	/**
//	 * Logger.
//	 */
//    private static final Logger getLogger() {
//        return Logger.getLogger(ApplicationBootstrap.class.getName());
//    }	
}
