package es.siani.gridop.data;

import java.beans.XMLEncoder;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletContext;

import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import es.siani.gridop.GridopGlobals;
import es.siani.gridop.model.BaseItem;

public class DataProviderHelper {
	
	/** Servlet context of the webapp is nedeed to find the resources. */
	private static ServletContext context = null;
	
	/** Cache of SqlSessions. It stores the last opened for each environment. */
	private static HashMap<String, SqlSessionFactory> sessionsCache = new HashMap<String, SqlSessionFactory>();
	
	
	/**
	 * Constructor.
	 * This class cannot be instantiated.
	 */
	private DataProviderHelper() {
		return;
	}
	
	
	/**
	 * Set the ServletContext of the Web Application.
	 */
	public static void initContext(ServletContext servletContext) {
		context = servletContext;
		return;
	}
	
	
	/**
	 * Get an instance of SqlSessionFactory that corresponds to the default 'environment'.
	 * This is used by MyBatis in order to run the sql statements.
	 */
	public static SqlSessionFactory getSQLSessionFactory() {
		return getSQLSessionFactory(GridopGlobals.STORE_DEFAULT_ENVIRONMENT);
	}
	
	
	/**
	 * Get the SqlSessionFactory instance.
	 * This is used by MyBatis in order to run the sql statements.
	 */
	public static SqlSessionFactory getSQLSessionFactory(String environment) {
		
		SqlSessionFactory sqlSession = sessionsCache.get(environment);
		
		if (sqlSession == null) {
			try {
				BufferedReader reader = new BufferedReader(new InputStreamReader(
						context.getResourceAsStream("/WEB-INF/mybatis/config.xml")));
				sqlSession = new SqlSessionFactoryBuilder().build(reader, environment);
				sessionsCache.put(environment, sqlSession);
				
			} catch(Exception ex) {
				getLogger().log(Level.SEVERE, "Trying to create the SqlSessionFactory of mybatis.");
			}
		}
		
		return sqlSession;
	}
	
	
	/**
	 * Encode a Java bean into XML text.
	 */
	public static String encodeItem(BaseItem item) {
		
		try {
			ByteArrayOutputStream bstream = new ByteArrayOutputStream();
			XMLEncoder encoder = new XMLEncoder(bstream);
			encoder.writeObject(item);
			encoder.close();
			return bstream.toString("UTF-8");
		} catch (Exception ioex) {
			getLogger().log(Level.SEVERE, "Trying to encode the item into XML text.");
		}
		
		return "";
	}
	
	
	/**
	 * Logger.
	 */
    private static final Logger getLogger() {
        return Logger.getLogger(DataProviderHelper.class.getName());
    }
}
