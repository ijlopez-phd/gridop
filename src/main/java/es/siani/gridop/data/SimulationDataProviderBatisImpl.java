package es.siani.gridop.data;

import java.util.Date;
import java.util.List;
import org.apache.ibatis.session.SqlSession;
import es.siani.gridop.model.Simulation;

public class SimulationDataProviderBatisImpl extends DataProviderBatisImpl<Simulation> implements SimulationDataProvider {
	
	
	/**
	 * Constructor.
	 */
	public SimulationDataProviderBatisImpl(Class<Simulation> dataClass) {
		super(dataClass);
		return;
	}
	
	/**
	 * Delete finished simulations.
	 */
	public void deleteFinishedSimulations() throws GridopDataException {
		
		SqlSession session = DataProviderHelper.getSQLSessionFactory().openSession();
		
		try {
			session.delete("deleteFinishedSimulations");
			session.commit();
		} catch (Exception ex) {
			throw new GridopDataException(ex);
		} finally {
			session.close();
		}
		
		return;
	}


	/**
	 * Get running simulations.
	 */
	public List<Simulation> getRunning() throws GridopDataException {
		
		SqlSession session = DataProviderHelper.getSQLSessionFactory().openSession();
		
		List<Simulation> simulations;
		try {
			simulations = session.selectList("getRunningSimulations");
		} catch (Exception ex) {
			throw new GridopDataException(ex);
		} finally {
			session.close();
		}
		
		return simulations;
	}	
	
	
	/**
	 * Set stop timestamp of a simulation object.
	 */
	public void setStopTimestamp(int idSimulation, Date stopTime) throws GridopDataException {
		SqlSession session = DataProviderHelper.getSQLSessionFactory().openSession();
		
		try {
			Simulation simulation = (Simulation)session.selectOne("getSimulationById", idSimulation);
			simulation.setStopped(stopTime);
			session.update("setSimulationStopTimestamp", simulation);
			session.commit();
		} catch (Exception ex) {
			throw new GridopDataException(ex);
		} finally {
			session.close();
		}
		
		return;
	}
}
