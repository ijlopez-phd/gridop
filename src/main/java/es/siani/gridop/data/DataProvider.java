package es.siani.gridop.data;

import java.util.List;
import es.siani.gridop.GridopException;
import es.siani.gridop.model.BaseItem;

public interface DataProvider<T extends BaseItem> {
	
	public List<T> getAll() throws GridopException;
	
	
	public T getFirst() throws GridopDataException;
	
	
	public T getById(int id) throws GridopDataException;
	
	
	public T getByCode(String code) throws GridopDataException;
	
	
	public void delete(int id) throws GridopDataException;
	
	
	public void save(T item) throws GridopDataException;
	
	
	public void addListener(DataProviderListener<T> listener);
	
	
	public void notifyItemAction(int actionType, T item);
}
