package es.siani.gridop.data;

public interface DataProviderListener<T> {
	
	public final int GRIDOP_ITEM_SAVED = 0;
	public final int GRIDOP_ITEM_DELETED = 1;
	public final int GRIDOP_ITEM_UPDATED = 2;
	
	
	/**
	 * Called when an item operation is performed on the DataProvider.
	 */
	public void itemChanged(int actionType, T item);
	
}
