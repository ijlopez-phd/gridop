package es.siani.gridop.data;

import java.util.HashMap;
import java.util.Map;
import org.apache.ibatis.session.SqlSession;
import es.siani.gridop.model.Client;
import es.siani.gridop.model.ClientsGroup;

public class ClientDataProviderBatisImpl extends DataProviderBatisImpl<Client>  {

	/**
	 * Constructor.
	 */
	public ClientDataProviderBatisImpl(Class<Client> dataClass) {
		super(dataClass);
		return;
	}
	
	
	/**
	 * Save a Client Item, including the relationship it has with the ClientsGroup entities.
	 */
	@Override
	public void save(Client item) throws GridopDataException {
		
		SqlSession session = DataProviderHelper.getSQLSessionFactory().openSession();
		
		try {
			if (item.getId() == null) {
				// The item hasn't been stored yet.
				session.insert("insertClient", item);
			} else {
				// The item is being updated
				session.update("updateClient", item);
				session.delete("removeClientFromAllClientsGroups", item.getId());
			}
			
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("clientId", item.getId());
			for (ClientsGroup group : item.getClientsGroups()) {
				params.put("clientsGroupId", group.getId());
				session.insert("addClientToClientsGroup", params);
				params.remove("clientsGroupId");
			}
			
			session.commit();
		} catch (Exception ex) {
			throw new GridopDataException(ex);
		} finally {
			session.close();
		}
		
		return;
	}
	
	

}
