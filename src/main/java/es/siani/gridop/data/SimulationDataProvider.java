package es.siani.gridop.data;

import java.util.Date;
import java.util.List;

import es.siani.gridop.model.Simulation;

public interface SimulationDataProvider extends DataProvider<Simulation> {
	
	public List<Simulation> getRunning() throws GridopDataException;
	
	public void setStopTimestamp(int idSimulation, Date stopTime) throws GridopDataException;
	
	
	public void deleteFinishedSimulations() throws GridopDataException;
}
