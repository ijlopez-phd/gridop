package es.siani.gridop.data;

import es.siani.gridop.GridopException;

@SuppressWarnings("serial")
public class GridopDataException extends GridopException {

	
	/**
	 * Constructor.
	 */
	public GridopDataException() {
		super();
	}
	
	
	/**
	 * Constructor.
	 */
	public GridopDataException(Throwable t) {
		super(t);
	}
	
	
	/**
	 * Constructor.
	 */
	public GridopDataException(String message) {
		super(message);
	}
	
	
	/**
	 * Constructor.
	 */
	public GridopDataException(String message, Throwable t) {
		super(message, t);
	}

}
