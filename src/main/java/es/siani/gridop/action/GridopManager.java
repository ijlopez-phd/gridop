package es.siani.gridop.action;

import javax.inject.Inject;

import es.siani.gridop.GridopException;
import es.siani.gridop.data.DataProvider;
import es.siani.gridop.model.GridOperator;

public class GridopManager {
	
	private final DataProvider<GridOperator> dao;
	
	
	/**
	 * Constructor.
	 */
	@Inject
	public GridopManager(DataProvider<GridOperator> dao) {
		this.dao = dao;
		return;
	}
	
	
	/**
	 * Get the Grid Operator item.
	 */
	public GridOperator getGridOperator() throws GridopException {
		GridOperator operator = this.dao.getFirst();
		if (operator == null) {
			operator = new GridOperator();
		}
		
		return operator;
	}
	
	
	/**
	 * Save the Grid Operator item.
	 */
	public void saveGridOperator(GridOperator gridOperator) throws GridopException {
		this.dao.save(gridOperator);
		return;
	}

}
