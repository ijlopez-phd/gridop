package es.siani.gridop.action;

import java.io.ByteArrayInputStream;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.inject.Inject;

import es.siani.simpledr.AsdrException;
import es.siani.simpledr.AsdrUtils;
import es.siani.simpledr.model.AsdrProgram;
import es.siani.gridop.GridopException;
import es.siani.gridop.data.DataProvider;
import es.siani.gridop.data.DataProviderListener;
import es.siani.gridop.model.Scenario;
import es.siani.gridop.model.GridopFile;

public class ScenarioManager {
	
	private final DataProvider<Scenario> dao;
	
	
	/**
	 * Constructor.
	 */
	@Inject
	public ScenarioManager(DataProvider<Scenario> dao) {
		this.dao = dao;
		return;
	}
	
	
	/**
	 * Check if the input parameter contains a valid definition of a 
	 * DR program.
	 */
	public GridopFile getProgram(String content) {
		
		GridopFile program = null;
		try {
			AsdrProgram asdrProgram = AsdrUtils.createProgram(new ByteArrayInputStream(content.getBytes()));
			program = new GridopFile(asdrProgram.getName(), content);
		} catch (AsdrException e) {
			getLogger().log(Level.SEVERE, "The file does not contain a valid DR Program.");
		}
		
		return program;
	}
	
	
	/**
	 * Get all Scenario items stored in the database.
	 */
	public List<Scenario> getAllScenarios() throws GridopException {
		return this.dao.getAll();
	}
	
	
	/**
	 * Delete a Scenario item.
	 */
	public void deleteScenario(int idScenario) throws GridopException {
		this.dao.delete(idScenario);
		return;
	}
	
	
	/**
	 * Save a Scenario item.
	 */
	public void saveScenario(Scenario scenario) throws GridopException {
		
		GridopFile program = scenario.getProgram();
		if ((program.getName() == null) || (program.getContent() == null)) {
			scenario.setProgram(null);
		}
		
		this.dao.save(scenario);
		
		return;
	}
	
	
	/**
	 * Get a Scenario item by using the Id property.
	 */
	public Scenario getScenarioById(int id) throws GridopException {
		return this.dao.getById(id);
	}
	
	
	/**
	 * Get a Scenario item by using the Code property.
	 */
	public Scenario getScenarioByCode(String code) throws GridopException {
		return this.dao.getByCode(code);
	}
	
	
	/**
	 * Add a listener to this data provider.
	 */
	public void addListener(DataProviderListener<Scenario> listener) {
		this.dao.addListener(listener);
		return;
	}
	
	
	/**
	 * Logger.
	 */
	private static final Logger getLogger() {
		return Logger.getLogger(ScenarioManager.class.getName());
	}

}
