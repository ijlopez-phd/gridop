package es.siani.gridop.action;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.inject.Inject;

import es.siani.gridop.GridopException;
import es.siani.gridop.data.DataProvider;
import es.siani.gridop.data.DataProviderListener;
import es.siani.gridop.data.GridopDataException;
import es.siani.gridop.model.Client;
import es.siani.gridop.model.ClientEvent;
import es.siani.gridop.model.ClientsGroup;
import es.siani.gridop.xmpp.SimpleXmpp;

public class ClientManager {
	
	private final DataProvider<Client> dao;
	private final DataProvider<ClientEvent> eventDao;
	private final DataProvider<ClientsGroup> clientsGroupDao;
	private final SimpleXmpp xmpp;
	
	
	/**
	 * Constructor.
	 */
	@Inject
	public ClientManager(
			DataProvider<Client> dao, DataProvider<ClientEvent> eventDao,
			DataProvider<ClientsGroup> clientsGroupDao, SimpleXmpp xmpp) {
		this.dao = dao;
		this.eventDao = eventDao;
		this.clientsGroupDao = clientsGroupDao;
		this.xmpp = xmpp;
		return;
	}
	
	
	/**
	 * Get all Client items stored in the database.
	 */
	public List<Client> getAllClients() throws GridopException {
		return this.dao.getAll();
	}
	
	
	/**
	 * Get all ClientsGroup itmes stored in the database.
	 */
	public List<ClientsGroup> getAllClientsGroups() throws GridopException {
		return this.clientsGroupDao.getAll();
	}
	
	
	/**
	 * Update a client.
	 */
	public Client updateClient(Client client) throws GridopException {
		
		// Update the Client information in the datastore.		
		Client storeClient = dao.getById(client.getId());
		storeClient.setCode(client.getCode());
		storeClient.setClientsGroups(client.getClientsGroups());
		dao.save(storeClient);
		
		// Register this event in the datastore
		try {
			this.eventDao.save(new ClientEvent(storeClient));
		} catch (GridopDataException ex) {
			getLogger().log(Level.WARNING, "Updating Client event has not been registered.");
		}
		
		return storeClient;	
		
	}
	
	
	/**
	 * Create Clients Group.
	 */
	public ClientsGroup createClientsGroup(ClientsGroup clientsGroup) throws GridopException {
		
		// Save the Clients Group in the datastore.
		ClientsGroup storeClientsGroup = new ClientsGroup();
		storeClientsGroup.copy(clientsGroup);
		clientsGroupDao.save(storeClientsGroup);
		
		return storeClientsGroup;
	}
	
	
	/**
	 * Register a new client.
	 */
	public Client createClient(Client client) throws GridopException {
		
		Client storeClient = new Client();
		storeClient.copy(client);

		// Create the XMPP accounts related to the client.
		xmpp.register(storeClient);		

		// Save the client in the datastore.
		try {
			dao.save(storeClient);
		} catch (GridopException ex) {
			xmpp.deregister(storeClient);
			throw ex;
		}
		
	
		// Register this event in the store.
		try {
			this.eventDao.save(new ClientEvent(storeClient));
		} catch (GridopDataException ex) {
			getLogger().log(Level.WARNING, "Creating Client event has not been registered.");
		}
		
		return storeClient;
	}
	
	
	/**
	 * Deregister a client.
	 */
	public void deleteClient(int idClient) throws GridopException {
		
		Client client = this.dao.getById(idClient);
		if (client == null) {
			throw new IllegalArgumentException();
		}
		
		// Delete the client from the datastore.
		this.dao.delete(idClient);
		
		// Delete the XMPP accounts of the client.
		xmpp.deregister(client);
		
		return;
	}
	
	
	/**
	 * Add a listener to this data provider.
	 */
	public void addListener(DataProviderListener<Client> listener) {
		this.dao.addListener(listener);
		return;
	}
	
	
	/**
	 * Logger.
	 */
	private static final Logger getLogger() {
		return Logger.getLogger(AspemManager.class.getName());
	}	

}
