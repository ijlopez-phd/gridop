package es.siani.gridop.action;

import java.util.List;
import java.util.logging.Logger;
import javax.inject.Inject;
import es.siani.gridop.GridopException;
import es.siani.gridop.data.DataProvider;
import es.siani.gridop.data.GridopDataException;
import es.siani.gridop.model.Aspem;
import es.siani.gridop.model.AspemEvent;

public class AspemManager {
	
	private final DataProvider<Aspem> dao;
	private final DataProvider<AspemEvent> eventDao;
	private final AspemServicesCaller wsCaller;
	
	
	/**
	 * Constructor.
	 */
	@Inject
	public AspemManager(DataProvider<Aspem> dao, DataProvider<AspemEvent> eventDao, AspemServicesCaller wsCaller) {
		this.dao = dao;
		this.eventDao = eventDao;
		this.wsCaller = wsCaller;
		return;
	}
	
	
	/**
	 * Get all Aspem items stored in the database.
	 */
	public List<Aspem> getAllAspems() throws GridopException {
		return this.dao.getAll();
	}
	
	
	/**
	 * Get Aspem by id.
	 */
	public Aspem getAspemById(int id) throws GridopException {
		return this.dao.getById(id);
	}
	
	
	/**
	 * Update the status of all the Aspems.
	 */
	public void updateAspemsStatus() throws GridopException {
		List<Aspem> aspems = this.dao.getAll();
		for (Aspem aspem : aspems) {
			try {
				boolean status = this.wsCaller.getStatus(aspem);
				if (aspem.getStatus() != status) {
					this.updateAspemStatus(aspem, status);
				}
			} catch (GridopException ex) {
				getLogger().severe("The status of ASPEM could not be saved.");
				throw new GridopException(ex);
			}
		}
		
		return;
	}
	
	
	/**
	 * Update the Status field of the ASPEM.
	 */
	public void updateAspemStatus(Aspem aspem, boolean status) throws GridopException {
		
		// Update the status field
		aspem.setStatus(status);
		dao.save(aspem);		
		
		// Register this event.
		try {
			this.eventDao.save(new AspemEvent(aspem));
		} catch (GridopDataException ex) {
			getLogger().warning("New status ASPEM event has not been registered.");
		}
		
		return;
		
	}
	
	
	/**
	 * Update the Status field of an Aspem.
	 */
	public void updateAspemStatus(int id, boolean status) throws GridopException {
		this.updateAspemStatus(dao.getById(id), status);
		return;
	}
	
	
	/**
	 * Update the data of an ASPEM, or creates a new one if it doesn't
	 * exist.
	 */
	public int createUpdateAspem(Aspem aspem) throws GridopException {
		
		Aspem storeAspem;
		if (aspem.getId() != null) {
			// The ASPEM is supposed to exist in the database.
			storeAspem = dao.getById(aspem.getId());
		} else {
			storeAspem = new Aspem();
		}
		
		storeAspem.copy(aspem);
		dao.save(storeAspem);
		
		// Register this event
		try {
			this.eventDao.save(new AspemEvent(storeAspem));
		} catch (GridopDataException ex) {
			getLogger().warning("Updating ASPEM event has not been registered.");
		}
		
		return storeAspem.getId();
	}
	
	
	/**
	 * Logger.
	 */
	private static final Logger getLogger() {
		return Logger.getLogger(AspemManager.class.getName());
	}		
}
