package es.siani.gridop.action;

import java.util.List;

import javax.inject.Inject;

import es.siani.gridop.GridopException;
import es.siani.gridop.data.DataProvider;
import es.siani.gridop.model.Simulator;

public class MasterDataManager {
	
	private final DataProvider<Simulator> daoSimulators;
	
	
	/**
	 * Constructor.
	 */
	@Inject
	public MasterDataManager(DataProvider<Simulator> daoSimulators) {
		this.daoSimulators = daoSimulators;
		return;
	}
	
	
	/**
	 * Get all simulators.
	 */
	public List<Simulator> getSimulators() throws GridopException {
		return this.daoSimulators.getAll();
	}
}
