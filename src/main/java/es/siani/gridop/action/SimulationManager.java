package es.siani.gridop.action;

import static es.siani.gridop.config.Configuration.PROP_ASPEM_CONTEXT_PATH;
import static es.siani.gridop.config.Configuration.PROP_ASPEM_RESOURCE;

import java.io.ByteArrayInputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.inject.Inject;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.webapp.WebAppContext;

import es.siani.energyagents.AgentsPlatform;
import es.siani.energyagents.EnergyAgentsException;
import es.siani.energyagents.GridopContainer;
import es.siani.energyagents.injection.InjectionUtils;
import es.siani.energyagents.model.PauseResult;
import es.siani.gridop.GridopException;
import es.siani.gridop.config.Configuration;
import es.siani.gridop.data.DataProvider;
import es.siani.gridop.data.GridopDataException;
import es.siani.gridop.data.SimulationDataProvider;
import es.siani.gridop.model.Aspem;
import es.siani.gridop.model.PauseActionResult;
import es.siani.gridop.model.Scenario;
import es.siani.gridop.model.Simulation;
import es.siani.gridop.model.SimulationParams;
import es.siani.simpledr.AsdrException;
import es.siani.simpledr.AsdrUtils;

public class SimulationManager {
	
	private final SimulationDataProvider simulationDao;
	private final DataProvider<Scenario> scenarioDao;
	private final AgentsPlatform agentsPlatform;
	private final Configuration config;
	private GridopContainer gridopContainer;
	private List<AspemServer> runningAspemServers;
	
	
	/**
	 * Constructor.
	 */
	@Inject
	public SimulationManager(
			SimulationDataProvider simulationDao,
			DataProvider<Scenario> scenarioDao,
			AgentsPlatform agentsPlatform,
			Configuration config) {
		this.simulationDao = simulationDao;
		this.scenarioDao = scenarioDao;
		this.agentsPlatform = agentsPlatform;
		this.config = config;
		this.runningAspemServers = new ArrayList<AspemServer>();
		
		
		return;
	}
	
	
	/**
	 * Get all registered simulations.
	 */
	public List<Simulation> getAllSimulations() throws GridopException {
		try {
			return this.simulationDao.getAll();
		} catch (GridopDataException ex) {
			throw new GridopException(ex);
		}
	}
	
	
	/**
	 * Get running simulations.
	 */
	public List<Simulation> getRunningSimulations() throws GridopException {
		try {
			return this.simulationDao.getRunning();
		} catch (GridopDataException ex) {
			throw new GridopException(ex);
		}
	}
	
	
	/**
	 * Set up the context necessary to initiate a new simulation.
	 */
	public int simulationInitiated(String scenarioCode, SimulationParams simulationParams) throws GridopException {
		
		// Get the scenario to which the simulation is going to be attached.
		Scenario scenario = this.scenarioDao.getByCode(scenarioCode);
		
		// Fulfill and store the new Simulation object
		Simulation simulation = new Simulation();
		simulation.setScenario(scenario);
		simulation.setStarted(new Date());
		this.simulationDao.save(simulation);
		

		// Configure the Jade platform in order for it to support a high number of agents.
		if (config.get(Configuration.PROP_JADE_DF_PATH) != null) {
			this.agentsPlatform.setProperty(
					AgentsPlatform.PROP_JADE_DF_PATH,
					this.config.get(Configuration.PROP_JADE_DF_PATH));
		}
		
		// Init the main container of the software agents.		
		this.gridopContainer = this.agentsPlatform.createGridopContainer(null);
		
		// Write the simulation params into the Configuration object of the EnergyAgents module.
		setupEnergyAgentsModule(simulationParams);

		// Inform the Gridop agent about the new simulation.
		try {
			this.agentsPlatform.informSimulationStarted(
					simulation.getId(),
					simulation.getScenario().getCode(),
					simulation.getScenario().getStoreName(),
					AsdrUtils.createProgram(
							new ByteArrayInputStream(scenario.getProgram().getContent().getBytes())));
		} catch (EnergyAgentsException | AsdrException ex) {
			this.simulationDao.setStopTimestamp(simulation.getId(), new Date());
			throw new GridopException(ex);
		}
	
		
		return simulation.getId();
	}
	
	
	/**
	 * If necessary, override configuration parameters of the EnergyAgents' module.
	 */
	@SuppressWarnings("static-access")
	private void setupEnergyAgentsModule(SimulationParams simParams) {
		
		if (simParams == null) {
			return;
		}
		
		es.siani.energyagents.config.Configuration eaConfig = 
				InjectionUtils.injector().getInstance(es.siani.energyagents.config.Configuration.class);
		
		if (simParams.getNumberDirectories() != null) {
			eaConfig.set(eaConfig.PROP_AUCTION_DF_AGENTS, simParams.getNumberDirectories().toString());
		}
		
		if (simParams.getOverbookingFactor() != null) {
			eaConfig.set(eaConfig.PROP_AUCTION_OVERBOOKING_FACTOR, simParams.getOverbookingFactor().toString());
		}
		
		eaConfig.set(eaConfig.PROP_AUCTION_USE_START_PRICE, Boolean.toString(simParams.getUsingStartPrice()));
		eaConfig.set(eaConfig.PROP_AUCTION_USE_RANDOM, Boolean.toString(simParams.getUsingRandomDistribution()));
		eaConfig.set(eaConfig.PROP_AUCTION_CF, Integer.toString(simParams.getConstantF()));
		
		return;
	}
	
	
	/**
	 * Shut down the simulation context.
	 */
	public void simulationFinished(int idSimulation) throws GridopException {
		this.simulationDao.setStopTimestamp(idSimulation, new Date());
		
		// Inform the Gridop agent that the simulation is over.
		try {
			this.agentsPlatform.informSimulationFinished(idSimulation);
		} catch (EnergyAgentsException ex) {
			throw new GridopException(ex);
		}
		
		// Stop the ASPEMS' servers.
		for (AspemServer server : this.runningAspemServers) {
			server.stop();
		}
		this.runningAspemServers.clear();
		
		// Shut down the main container.
		this.gridopContainer.kill();
		
		return;
	}
	
	
	/**
	 * Informs the Gridop agent that the simulation is paused.
	 * The realTime and modelTime are expressed in XML format.
	 */
	public PauseActionResult simulationPaused(int idSimulation, String strRealTime, String strModelTime) throws GridopException {
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss z");
		Date realTime;
		Date modelTime;
		try {
			realTime = sdf.parse(strRealTime);
			modelTime = sdf.parse(strModelTime);
		} catch (ParseException ex) {
			getLogger().log(Level.SEVERE, "Error during converting strings to the Date java type.");
			throw new GridopException(ex);
		}
		
		return this.simulationPaused(idSimulation, realTime, modelTime);
	}
	
	
	/**
	 * Informs the Gridop agent that the simulation is paused.
	 */
	public PauseActionResult simulationPaused(int idSimulation, Date realTime, Date modelTime) throws GridopException {
		
		// Inform the Gridop agent that the simulation is paused.
		PauseResult pauseResult;
		try {
			pauseResult = this.agentsPlatform.informSimulationPaused(idSimulation, realTime, modelTime);
		} catch (EnergyAgentsException ex) {
			throw new GridopException(ex);
		}
		
		PauseActionResult pause =
				new PauseActionResult(pauseResult.getNextPause(), pauseResult.getDistributeActionTriggered());
		
		return pause;
	}
	
	
	/**
	 * Delete a Simulation item.
	 */
	public void deleteSimulation(int idSimulation) throws GridopException {
		this.simulationDao.delete(idSimulation);
	}
	
	
	/**
	 * Delete all finished simulations.
	 */
	public void deleteFinishedSimulations() throws GridopException {
		this.simulationDao.deleteFinishedSimulations();
	}
	
	
	/**
	 * Run an embedded instance of the Jetty server.
	 * This instance represents an ASPEM.
	 */
	public void runAspemServer(Aspem aspem) {
		
		// Run the server on a separate thread.
		AspemServer server = new AspemServer(aspem.getCode(), aspem.getName(), aspem.getPort());
		server.start();
		
		// Store the description of the running server.
		this.runningAspemServers.add(server);
		
		return;
	}
	
	
	/**
	 * Logger.
	 */
	private static final Logger getLogger() {
		return Logger.getLogger(SimulationManager.class.getName());
	}
	
	
	/**
	 * Class AspemServer.
	 */
	private class AspemServer {
		
		private final String code; /** Code of the ASPEM. It must be unique. */
		private final String name; /** ASPEM descriptive name. */
		private final int port; /** Port at which should run the server. */
		private Server server; /** Reference to the Jetty Server instance. */
		
		
		/**
		 * Constructor.
		 */
		public AspemServer(String code, String name, int port) {
			this.code = code;
			this.name = name;
			this.port = port;
			return;
		}
		
		
		/**
		 * Start the Jetty server.
		 */
		public void start() {
			
			this.server = new Server();
			
			// Add the server connector.
			ServerConnector http = new ServerConnector(this.server);
			http.setHost("localhost");
			http.setPort(this.port);
			http.setIdleTimeout(30000);
			server.addConnector(http);
			
			// Add the handler of the web application.
			WebAppContext webapp = new WebAppContext();
			webapp.setContextPath(SimulationManager.this.config.get(PROP_ASPEM_CONTEXT_PATH));
			String resourcePath = SimulationManager.this.config.get(PROP_ASPEM_RESOURCE);
			if (resourcePath.toLowerCase().endsWith(".war")) {
				webapp.setWar(resourcePath);
			} else {
				webapp.setResourceBase(resourcePath);
				webapp.setDescriptor(resourcePath + "/WEB-INF/web.xml");				
			}
			webapp.setInitParameter("es.siani.aspem.code", this.code);
			webapp.setInitParameter("es.siani.aspem.name", this.name);
			webapp.setParentLoaderPriority(true);
			
			this.server.setHandler(webapp);
			this.server.setStopAtShutdown(true);
			
			try {
				// Start the server.
				this.server.start();
				
				// Have current thread waiting till server is done running.
				//server.join();				
			} catch (Exception e) {
				throw new IllegalStateException("Failed to start the Jetty server.", e);
			}
			
			return;
		}
		
		
		/**
		 * Stop the Jetty server.
		 */
		public void stop() {
			
			if (this.server == null) {
				return;
			}
			
			try {
				server.stop();
			} catch (Exception e) {
				throw new IllegalStateException("Failed to stop the Jetty server.", e);
			}
			
			return;
		}


		/**
		 * @see java.lang.Object#hashCode()
		 */
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + getOuterType().hashCode();
			result = prime * result + ((code == null) ? 0 : code.hashCode());
			return result;
		}

		
		/**
		 * @see java.lang.Object#equals(Object)
		 */
		@Override
		public boolean equals(Object obj) {
			if (this == obj) {
				return true;
			}
			if (obj == null) {
				return false;
			}
			if (!(obj instanceof AspemServer)) {
				return false;
			}
			AspemServer other = (AspemServer) obj;
			if (!getOuterType().equals(other.getOuterType())) {
				return false;
			}
			if (code == null) {
				if (other.code != null) {
					return false;
				}
			} else if (!code.equals(other.code)) {
				return false;
			}
			return true;
		}		
		

		/**
		 * Return the reference to the SimulationManager instance that wrappers
		 * the AspemServer.
		 */
		private SimulationManager getOuterType() {
			return SimulationManager.this;
		}
	}	
}



