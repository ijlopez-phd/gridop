package es.siani.gridop.action;

import es.siani.gridop.GridopException;
import es.siani.gridop.model.Aspem;

public interface AspemServicesCaller {
	
	/**
	 * Get the status of an Aspem.
	 */
	public boolean getStatus(Aspem aspem) throws GridopException;

}
