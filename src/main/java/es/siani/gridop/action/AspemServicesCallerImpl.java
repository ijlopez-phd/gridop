package es.siani.gridop.action;

import java.util.logging.Logger;
import javax.inject.Inject;
import es.siani.gridop.GridopException;
import es.siani.gridop.model.Aspem;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

public class AspemServicesCallerImpl implements AspemServicesCaller {
	
	private static final int HTTP_OK = ClientResponse.Status.OK.getStatusCode();
	
	private static final String MSG_GET_STATUS =
			"The status of the ASPEM could not retrieved. It is assumed that the ASPEM is down.";
	
	/**
	 * Constructor.
	 */
	@Inject
	public AspemServicesCallerImpl() {
		return;
	}

	
	/**
	 * @see es.siani.gridop.action.AspemServicesCaller#getStatus(Aspem)
	 */	
	public boolean getStatus(Aspem aspem) throws GridopException {
		
		if (aspem == null) {
			throw new IllegalArgumentException("The parameter 'aspem' must contain a valid instance of the class Aspem.");
		}
		
		Client client = Client.create();
		WebResource call = client.resource("localhost:" + aspem.getPort() + "/resources/aspem/status");
		ClientResponse response;
		try {
			response = call.get(ClientResponse.class);
			if (response.getStatus() != HTTP_OK) {
				getLogger().warning(MSG_GET_STATUS);
				return false;
			}
		} catch (Exception ex) {
			getLogger().warning(MSG_GET_STATUS);
			return false;
		}
		
		return Boolean.parseBoolean(response.getEntity(String.class));
	}
	
	
	/**
	 * Logger. 
	 */
    private static final Logger getLogger() {
        return Logger.getLogger(AspemServicesCallerImpl.class.getName());
    }	
}
