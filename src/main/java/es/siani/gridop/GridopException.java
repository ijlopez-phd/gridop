package es.siani.gridop;

@SuppressWarnings("serial")
public class GridopException extends Exception {
	
	/**
	 * Constructor.
	 */
	public GridopException() {
		super();
	}
	
	
	/**
	 * Constructor.
	 */
	public GridopException(Throwable t) {
		super(t);
	}
	
	
	/**
	 * Constructor.
	 */
	public GridopException(String message) {
		super(message);
	}
	
	
	/**
	 * Constructor.
	 */
	public GridopException(String message, Throwable t) {
		super(message, t);
	}

}
