package es.siani.gridop.model;

public class GridOperator extends BaseItem {
	
	protected String description;
	
	
	/**
	 * Get the description field of the item.
	 */
	public String getDescription() {
		return description;
	}
	
	
	/**
	 * Set the description field of the item.
	 */
	public void setDescription(String description) {
		this.description = description;
	}
}
