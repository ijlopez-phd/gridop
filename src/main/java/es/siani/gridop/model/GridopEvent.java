package es.siani.gridop.model;

import java.util.Date;

public class GridopEvent<T extends BaseItem> {
	
	protected T item;
	protected Date stamp;
	protected String description;
	
	
	/**
	 * Constructor.
	 */
	public GridopEvent() {
		this.stamp = new Date();
		return;
	}
	
	
	/**
	 * Constructor.
	 */
	public GridopEvent(T item, String description) {
		this.stamp = new Date();
		this.item = item;
		this.description = description;
		return;
	}


	/**
	 * Get the Item.
	 */
	public T getItem() {
		return item;
	}


	/**
	 * Set the Item.
	 */
	public void setItem(T item) {
		this.item = item;
	}


	/**
	 * Get the Stamp.
	 */	
	public Date getStamp() {
		return stamp;
	}


	/**
	 * Get the Stamp.
	 */	
	public void setStamp(Date stamp) {
		this.stamp = stamp;
	}


	/**
	 * Get the Description.
	 */	
	public String getDescription() {
		return description;
	}


	/**
	 * Set the Description.
	 */	
	public void setDescription(String description) {
		this.description = description;
	}
}
