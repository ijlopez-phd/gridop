package es.siani.gridop.model;

import java.io.Serializable;

@SuppressWarnings("serial")
public class Scenario extends BaseItem implements Serializable {
	
	protected String code;
	protected String description;
	protected String storeName;
	protected GridopFile program;
	
	
	/**
	 * Get the code of the Scenario.
	 */
	public String getCode() {
		return this.code;
	}
	
	
	/**
	 * Set the code of the Scenario;
	 */
	public void setCode(String code) {
		this.code = code;
	}
	
	
	/**
	 * Get the name of the Scenario.
	 */
	public String getStoreName() {
		return this.storeName;
	}
	
	
	/**
	 * Set the name of the Scenario.
	 */
	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}
	
	
	/**
	 * Get the description of the Scenario.
	 */
	public String getDescription() {
		return this.description;
	}
	
	
	/**
	 * Set the description of the Scenario.
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	
	
	/**
	 * Get the DR program.
	 */
	public GridopFile getProgram() {
		return this.program;
	}
	
	
	/**
	 * Set the DR program.
	 */
	public void setProgram(GridopFile program) {
		this.program = program;
	}
	
	
	/**
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((code == null) ? 0 : code.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}


	/**
	 * @see java.lang.Object#equals(Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Scenario)) {
			return false;
		}
		Scenario other = (Scenario) obj;
		if (code == null) {
			if (other.code != null) {
				return false;
			}
		} else if (!code.equals(other.code)) {
			return false;
		}
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		
		return true;
	}
}