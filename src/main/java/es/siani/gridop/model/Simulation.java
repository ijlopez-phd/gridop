package es.siani.gridop.model;

import java.io.Serializable;
import java.util.Date;

@SuppressWarnings("serial")
public class Simulation extends BaseItem implements Serializable {
	
	protected Date started;
	protected Date stopped;
	protected Scenario scenario;
	
	
	/**
	 * Constructor.
	 */
	public Simulation() {
		return;
	}
	
	
	/**
	 * Get the time at which the simulation started.
	 */
	public Date getStarted() {
		return started;
	}
	
	
	/**
	 * Set the time at which the simulation started. 
	 */
	public void setStarted(Date started) {
		this.started = started;
	}
	
	
	/**
	 * Set the Scenario related to the Simulation. 
	 */
	public Scenario getScenario() {
		return scenario;
	}
	
	
	/**
	 * Get the Id of the Scenario related to the Simulation.
	 */
	public void setScenario(Scenario scenario) {
		this.scenario = scenario;
	}	
	
	
	/**
	 * Set the time at which the simulation stopped.
	 */
	public void setStopped(Date stopped) {
		this.stopped = stopped;
	}
	
	
	/**
	 * Get the time at which the simulation stopped.
	 */
	public Date getStopped() {
		return stopped;
	}
}
