package es.siani.gridop.model;

import java.io.Serializable;
import java.util.Set;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

@SuppressWarnings("serial")
@XmlRootElement(name="client")
@XmlType(propOrder={"code", "xmppAddress", "remoteXmppAddress", "clientsGroups"})
@XmlAccessorType(XmlAccessType.FIELD)
public class Client extends BaseItem implements Serializable {
	
	@XmlElement
	protected String code;
	
	@XmlElement
	protected String xmppAddress;
	
	@XmlElement
	protected String remoteXmppAddress;
	
	@XmlTransient
	protected Aspem aspem;
	
	@XmlElementWrapper(name="clientsGroups") @XmlElement(name="clientsGroup")
	protected Set<ClientsGroup> clientsGroups;
	
	
	/**
	 * Constructor.
	 */
	public Client() {
		return;
	}
	
	/**
	 * Get the Code.
	 */
	public String getCode() {
		return code;
	}
	
	
	/**
	 * Set the Code. 
	 */
	public void setCode(String code) {
		this.code = code;
	}
	
	
	/**
	 * Get the XmppAddress.
	 */
	public String getXmppAddress() {
		return xmppAddress;
	}
	
	
	/**
	 * Set the XmppAddress. 
	 */
	public void setXmppAddress(String xmppAddress) {
		this.xmppAddress = xmppAddress;
	}
	
	
	/**
	 * Get the RemoteXmppAddress.
	 */
	public String getRemoteXmppAddress() {
		return remoteXmppAddress;
	}
	
	
	/**
	 * Set the RemoteXmppAddress. 
	 */
	public void setRemoteXmppAddress(String remoteXmppAddress) {
		this.remoteXmppAddress = remoteXmppAddress;
	}	
	
	
	/**
	 * Get the ASPEM.
	 */
	public Aspem getAspem() {
		return aspem;
	}
	
	
	/**
	 * Set the ASPEM.
	 */
	public void setAspem(Aspem aspem) {
		this.aspem = aspem;
	}
	
	/** 
	 * Get the list of Clients Groups.
	 */
	public Set<ClientsGroup> getClientsGroups() {
		return clientsGroups;
	}
	
	
	/**
	 * Set the list of Clients Groups.
	 */
	public void setClientsGroups(Set<ClientsGroup> clientsGroups) {
		this.clientsGroups = clientsGroups;
		return;
	}	
	
	
	/**
	 * Copy properties of other Client item.
	 */
	public void copy(Client other) {
		this.code = other.code;
		this.xmppAddress = other.xmppAddress;
		this.remoteXmppAddress = other.remoteXmppAddress;
		this.aspem = other.aspem;
		this.clientsGroups = other.clientsGroups;
		return;
	}
}