package es.siani.gridop.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

@SuppressWarnings("serial")
@XmlRootElement(name = "aspem")
@XmlType(propOrder = {"code", "name", "host", "port"})
@XmlAccessorType(XmlAccessType.FIELD)
public class Aspem extends BaseItem implements Serializable {
	
	@XmlElement(name = "code")
	protected String code;
	
	@XmlElement(name = "name")
	protected String name;
	
	@XmlElement(name = "host")
	protected String host;
	
	@XmlElement(name = "port")
	protected Integer port;
	
	@XmlTransient
	protected Boolean status;
	
	
	/**
	 * Constructor.
	 */
	public Aspem() {
		return;
	}
	
	
	/**
	 * Get the Code.
	 */	
	public String getCode() {
		return code;
	}
	
	
	/**
	 * Set the Code.
	 */	
	public void setCode(String code) {
		this.code = code;
	}
	
	
	/**
	 * Get the Name.
	 */
	public String getName() {
		return name;
	}
	
	
	/**
	 * Set the Name.
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	
	/**
	 * Get the Host.
	 */
	public String getHost() {
		return this.host;
	}
	
	
	/**
	 * Set the Host.
	 */
	public void setHost(String host) {
		this.host = host;
		return;
	}
	
	
	/**
	 * Get the Url.
	 */
	public Integer getPort() {
		return this.port;
	}	
	
	
	/**
	 * Set the Url.
	 */
	public void setPort(Integer port) {
		this.port = port;
	}
	
	
	/**
	 * Get the Status.
	 */
	public Boolean getStatus() {
		return status;
	}
	
	
	/**
	 * Set the Status.
	 */
	public void setStatus(Boolean status) {
		this.status = status;
	}
	
	
	/**
	 * Copy properties of other Aspem item.
	 */
	public void copy(Aspem other) {
		this.code = other.code;
		this.name = other.name;
		this.port = other.port;
		this.status = other.status;
		
		return;
	}
}
