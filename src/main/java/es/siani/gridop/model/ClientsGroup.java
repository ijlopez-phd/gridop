package es.siani.gridop.model;

import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlAccessType;

import es.siani.gridop.model.BaseItem;

@XmlRootElement(name="clientsGroup")
@XmlType(propOrder={"code", "name", "description"})
@XmlAccessorType(XmlAccessType.FIELD)
public class ClientsGroup extends BaseItem {
	
	@XmlElement
	protected String code;
	
	@XmlElement
	protected String name;
	
	@XmlElement	
	protected String description;
	
	
	/**
	 * Get the code.
	 */
	public String getCode() {
		return code;
	}
	
	
	/**
	 * Set the code.
	 */
	public void setCode(String code) {
		this.code = code;
	}
	
	
	/**
	 * Get the Name.
	 */
	public String getName() {
		return name;
	}
	
	
	/**
	 * Set the Name.
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	
	/**
	 * Get the Description.
	 */
	public String getDescription() {
		return description;
	}
	
	
	/**
	 * Set the Description.
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	
	
	/**
	 * Copy the data fields of other ClientsGroup.
	 */
	public void copy(ClientsGroup o) {
		this.id = o.id;
		this.code = o.code;		
		this.name = o.name;
		this.description = o.description;
		return;
	}
	
	/**
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((code == null) ? 0 : code.hashCode());
		return result;
	}


	/**
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof ClientsGroup)) {
			return false;
		}
		ClientsGroup other = (ClientsGroup) obj;
		if (code == null) {
			if (other.code != null) {
				return false;
			}
		} else if (!code.equals(other.code)) {
			return false;
		}
		return true;
	}	
}