package es.siani.gridop.model;

public class SimulationParams {
	
	protected Integer numberDirectories;
	protected Float overbookingFactor;
	protected Boolean usingStartPrice;
	protected Boolean usingRandomDistribution;
	protected Integer constantF;
	
	
	/**
	 * Constructor.
	 */
	public SimulationParams() {
		return;
	}
	
	
	/**
	 * Constructor.
	 */
	public SimulationParams(
			Integer numberDirectories, Float overbookingFactor,
			Boolean usingStartPrice, Boolean usingRandomDistribution,
			Integer constantF) {
		this.numberDirectories = numberDirectories;
		this.overbookingFactor = overbookingFactor;
		this.usingStartPrice = usingStartPrice;
		this.usingRandomDistribution = usingRandomDistribution;
		this.constantF = constantF;
		return;
	}
	
	/**
	 * Set the number of Directory Facilitators used in the simulation.
	 */
	public void setNumberDirectories(int numberDirectories) {
		this.numberDirectories = numberDirectories;
		return;
	}
	
	
	/**
	 * Get the number of Directory Facilitators used in the simulation.
	 */
	public Integer getNumberDirectories() {
		return this.numberDirectories;
	}
	
	
	/**
	 * Set the overbooking factor used in the simulation.
	 */
	public void setOverbookingFactor(float overbookingFactor) {
		this.overbookingFactor = overbookingFactor;
		return;
	}
	
	
	/**
	 * Get the overbooking factor used in the simulation.
	 */
	public Float getOverbookingFactor() {
		return this.overbookingFactor;
	}
	
	
	/**
	 * Get the flag that indicates whether the start price should be used in the auctions.
	 */
	public Boolean getUsingStartPrice() {
		return this.usingStartPrice;
	}
	
	
	/**
	 * Set the flat that indicates whether the start price should be used in the auctions.
	 */
	public void setUsingStartPrice(Boolean usingStartPrice) {
		this.usingStartPrice = usingStartPrice;
		return;
	}
	
	
	/**
	 * Get the flag that indicates whether to use a randomized distribution of the buyers.
	 */
	public Boolean getUsingRandomDistribution() {
		return this.usingRandomDistribution;
	}
	
	
	/**
	 * Set the flat that indicates whether to use a randomized distribution of the buyers.
	 */
	public void setUsingRandomDistribution(Boolean usingRandomDistribution) {
		this.usingRandomDistribution = usingRandomDistribution;
		return;
	}
	
	
	/**
	 * Get the constant CF. 
	 */
	public Integer getConstantF() {
		return this.constantF;
	}
	
	
	/**
	 * Set the constant CF.
	 */
	public void setConstantF(Integer constantF) {
		this.constantF = constantF;
		return;
	}
}
