package es.siani.gridop.model;

import java.io.Serializable;
import java.util.Date;

import es.siani.gridop.data.DataProviderHelper;

@SuppressWarnings("serial")
public class AspemEvent extends BaseItem implements Serializable {
	
	protected Aspem aspem;
	protected Date stamp;
	protected String description;
	
	
	/**
	 * Constructor.
	 */
	public AspemEvent() {
		this.stamp = new Date();
		return;
	}
	
	
	/**
	 * Constructor.
	 */
	public AspemEvent(Aspem aspem) {
		this();
		this.aspem = aspem;
		this.description = DataProviderHelper.encodeItem(aspem);
		return;
	}	
	
	
	/**
	 * Get the ASPEM.
	 */
	public Aspem getAspem() {
		return aspem;
	}
	
	
	/**
	 * Set the ASPEM.
	 */
	public void setAspem(Aspem aspem) {
		this.aspem = aspem;
	}
	
	
	/**
	 * Get the Stamp.
	 */	
	public Date getStamp() {
		return stamp;
	}
	
	
	/**
	 * Set the Stamp.
	 */	
	public void setStamp(Date stamp) {
		this.stamp = stamp;
	}	
	
	
	/**
	 * Get the Description.
	 */	
	public String getDescription() {
		return description;
	}
	
	
	/**
	 * Set the Description.
	 */	
	public void setDescription(String description) {
		this.description = description;
	}
}
