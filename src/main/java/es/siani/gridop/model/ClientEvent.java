package es.siani.gridop.model;

import java.io.Serializable;
import java.util.Date;

import es.siani.gridop.data.DataProviderHelper;

@SuppressWarnings("serial")
public class ClientEvent extends BaseItem implements Serializable {

	protected Client client;
	protected Date stamp;
	protected String description;
	
	
	/**
	 * Constructor.
	 */
	public ClientEvent() {
		this.stamp = new Date();
		return;
	}
	
	
	/**
	 * Constructor.
	 */
	public ClientEvent(Client client) {
		this();
		this.client = client;
		this.description = DataProviderHelper.encodeItem(client);
		return;
	}


	/**
	 * Get the Client.
	 */
	public Client getClient() {
		return client;
	}


	/**
	 * Set the Client.
	 */
	public void setClient(Client client) {
		this.client = client;
	}


	/**
	 * Get the stamp.
	 */
	public Date getStamp() {
		return stamp;
	}


	/**
	 * Set the stamp.
	 */
	public void setStamp(Date stamp) {
		this.stamp = stamp;
	}


	/**
	 * Get the description.
	 */
	public String getDescription() {
		return description;
	}


	/**
	 * Set the description.
	 */
	public void setDescription(String description) {
		this.description = description;
	}
}
