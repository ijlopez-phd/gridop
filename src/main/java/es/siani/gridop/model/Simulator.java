package es.siani.gridop.model;

import java.io.Serializable;

@SuppressWarnings("serial")
public class Simulator extends BaseItem implements Serializable {
	
	protected String name = null;
	
	
	/**
	 * Constructor.
	 */
	public Simulator() {
		return;
	}
	
	/**
	 * Constructor.
	 */
	public Simulator(int id, String name) {
		this.id = id;
		this.name = name;
		return;
	}
	
	
	/**
	 * Get the name.
	 */
	public String getName() {
		return this.name;
	}
	
	
	/**
	 * Set the name.
	 */
	public void setName(String name) {
		this.name = name;
		return;
	}
	
	
	/**
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	
	/**
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public boolean equals(Object obj) {
		
		if (this == obj) {
			return true;
		}
		
		if (obj == null) {
			return false;
		}
		
		if (!(obj instanceof Simulator)) {
			return false;
		}
		
		Simulator other = (Simulator) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		
		if (name == null) {
			if (other.name != null) {
				return false;
			}
		} else if (!name.equals(other.name)) {
			return false;
		}
		
		return true;
	}	
}
