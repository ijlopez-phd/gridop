package es.siani.gridop.model;

public class GridopFile {
	
	protected String name;
	protected String content;
	
	/**
	 * Constructor.
	 */
	public GridopFile() {
		return;
	}
	
	
	/**
	 * Constructor.
	 */
	public GridopFile(String name, String content) {
		this.name = name;
		this.content = content;
		return;
	}


	/**
	 * Gets the Name.
	 */
	public String getName() {
		return name;
	}


	/**
	 * Sets the Name.
	 */
	public void setName(String name) {
		this.name = name;
	}


	/**
	 * Gets the Content.
	 */
	public String getContent() {
		return content;
	}


	/**
	 * Sets the content.
	 */
	public void setContent(String content) {
		this.content = content;
	}
	
	
	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return this.name;
	}
}
