package es.siani.gridop.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@XmlAccessorType(XmlAccessType.FIELD)
public abstract class BaseItem {

	@XmlElement
	protected Integer id;
	
	
	/**
	 * Get the Id.
	 */
	public Integer getId() {
		return id;
	}
	
	
	/**
	 * Set the Id.
	 */	
	public void setId(Integer id) {
		this.id = id;
	}	

}
