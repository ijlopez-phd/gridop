package es.siani.gridop.model;

public class PauseActionResult {
	
	/** Next time the GridOperator must be notified about a pause. */
	protected long nextPause;
	
	/** Tells whether DistributeActionEvents have been triggered during this call. */
	protected boolean distributeActionTriggered;
	
	
	/**
	 * Constructor.
	 */
	public PauseActionResult() {
		this.nextPause = -1;
		this.distributeActionTriggered = false;
	}
	
	
	/**
	 * Constructor.
	 */
	public PauseActionResult(long nextPause, boolean distributeActionTriggered) {
		this.nextPause = nextPause;
		this.distributeActionTriggered = distributeActionTriggered;
		return;
	}
	
	
	/**
	 * Get the NextPause.
	 */
	public long getNextPause() {
		return this.nextPause;
	}
	
	
	/**
	 * Set the NextPause.
	 */
	public void setNextPause(long nextPause) {
		this.nextPause = nextPause;
		return;
	}
	
	
	/**
	 * Get DistributeActionTriggered.
	 */
	public boolean getDistributeActionTriggered() {
		return this.distributeActionTriggered;
	}
	
	
	/**
	 * Set DistributeActionTriggered.
	 */
	public void setDistributeActionTriggered(boolean distributeActionTriggered) {
		this.distributeActionTriggered = distributeActionTriggered;
		return;
	}	

}
