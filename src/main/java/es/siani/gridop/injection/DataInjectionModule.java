package es.siani.gridop.injection;

import javax.inject.Singleton;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.Scopes;

import es.siani.gridop.action.SimulationManager;
import es.siani.gridop.data.ClientDataProviderBatisImpl;
import es.siani.gridop.data.DataProvider;
import es.siani.gridop.data.DataProviderBatisImpl;
import es.siani.gridop.data.SimulationDataProvider;
import es.siani.gridop.data.SimulationDataProviderBatisImpl;
import es.siani.gridop.model.Aspem;
import es.siani.gridop.model.AspemEvent;
import es.siani.gridop.model.Client;
import es.siani.gridop.model.ClientEvent;
import es.siani.gridop.model.ClientsGroup;
import es.siani.gridop.model.GridOperator;
import es.siani.gridop.model.Scenario;
import es.siani.gridop.model.Simulation;
import es.siani.gridop.model.Simulator;

public class DataInjectionModule extends AbstractModule {

	
	/**
	 * Dependency Injector module for the Manager objects.
	 */
	@Override
	protected void configure() {
		bind(SimulationManager.class).in(Scopes.SINGLETON);
		return;
	}
	
	
	/**
	 * Provider for instancing a Data Provider for the Aspem objects.
	 */
	@Provides @Singleton
	DataProvider<Aspem> getAspemDataProvider() {
		return new DataProviderBatisImpl<Aspem>(Aspem.class);
	}
	
	
	/**
	 * Provider for instancing a Batis implementation of an Aspem Data Provider.
	 */
	@Provides @Singleton
	DataProviderBatisImpl<Aspem> getAspemDataProviderBatis() {
		return new DataProviderBatisImpl<Aspem>(Aspem.class);
	}
	
	
	/**
	 * Provider for instancing a Data Provider for the AspemEvent objects.
	 */
	@Provides @Singleton
	DataProvider<AspemEvent> getAspemEventDataProvider() {
		return new DataProviderBatisImpl<AspemEvent>(AspemEvent.class);
	}
	
	
	/**
	 * Provider for instancing a Batis implementation of an AspemEvent Data Provider.
	 */
	@Provides @Singleton
	DataProviderBatisImpl<AspemEvent> getAspemEventDataProviderBatis() {
		return new DataProviderBatisImpl<AspemEvent>(AspemEvent.class);
	}	
	
	
	/**
	 * Provider for instancing a Data Provider for the Client objects.
	 */
	@Provides @Singleton
	DataProvider<Client> getClientDataProvider() {
		return new ClientDataProviderBatisImpl(Client.class);
	}
	
	
	/**
	 * Provider for instancing a Batis implementation of an Client Data Provider.
	 */
	@Provides @Singleton
	DataProviderBatisImpl<Client> getClientDataProviderBatis() {
		return new ClientDataProviderBatisImpl(Client.class);
	}
	
	@Provides @Singleton
	ClientDataProviderBatisImpl getClientDataProviderBatisImpl() {
		return new ClientDataProviderBatisImpl(Client.class);
	}
	
	/**
	 * Provider for instancing a Data Provider for the ClientEvent objects.
	 */
	@Provides @Singleton
	DataProvider<ClientEvent> getClientEventDataProvider() {
		return new DataProviderBatisImpl<ClientEvent>(ClientEvent.class);
	}
	
	
	/**
	 * Provider for instancing a Batis implementation of a ClientEvent object.
	 */
	@Provides @Singleton
	DataProviderBatisImpl<ClientEvent> getClientEventDataProviderBatis() {
		return new DataProviderBatisImpl<ClientEvent>(ClientEvent.class);
	}
	
	
	/**
	 * Provider for instancing a Data Provider for the ClientsGroup objects.
	 */
	@Provides @Singleton
	DataProvider<ClientsGroup> getClientsGroupDataProvider() {
		return new DataProviderBatisImpl<ClientsGroup>(ClientsGroup.class);
	}
	
	
	/**
	 * Provider for instancing a Batis implementation of a ClientsGroup object.
	 */
	@Provides @Singleton
	DataProviderBatisImpl<ClientsGroup> getClientsGroupDataProviderBatis() {
		return new DataProviderBatisImpl<ClientsGroup>(ClientsGroup.class);
	}	
	
	
	/**
	 * Provider for instancing a Data Provider for the Gridop object.
	 */
	@Provides @Singleton
	DataProvider<GridOperator> getGridOperatorDataProvider() {
		return new DataProviderBatisImpl<GridOperator>(GridOperator.class);
	}
	
	
	/**
	 * Provider for instancing a Batis implementation of an Gridop Data Provider.
	 */
	@Provides @Singleton
	DataProviderBatisImpl<GridOperator> getGridOperatorDataProviderBatis() {
		return new DataProviderBatisImpl<GridOperator>(GridOperator.class);
	}
	
	
	/**
	 * Provider for instancing a Data Provider for the Simulator objects.
	 */
	@Provides @Singleton
	DataProvider<Simulator> getSimulatorProvider() {
		return new DataProviderBatisImpl<Simulator>(Simulator.class);
	}
	
	
	/**
	 * Provider for instancing a Batis implementation of an Simulator Data Provider.
	 */
	@Provides @Singleton
	DataProviderBatisImpl<Simulator> getSimulatorDataProviderBatis() {
		return new DataProviderBatisImpl<Simulator>(Simulator.class);
	}
	
	
	/**
	 * Provider for instancing a Data Provider for the Scenario objects.
	 */
	@Provides @Singleton
	DataProvider<Scenario> getScenarioProvider() {
		return new DataProviderBatisImpl<Scenario>(Scenario.class);
	}
	
	
	/**
	 * Provider for instancing a Batis implementation of an Scenario Data Provider.
	 */
	@Provides @Singleton
	DataProviderBatisImpl<Scenario> getScenarioDataProviderBatis() {
		return new DataProviderBatisImpl<Scenario>(Scenario.class);
	}
	
	
	/**
	 * Provider for instancing a Data Provider for the Simulation objects.
	 */
	@Provides @Singleton
	SimulationDataProvider getSimulationProvider() {
		return new SimulationDataProviderBatisImpl(Simulation.class);
	}
	
	
	/**
	 * Provider for instancing a Batis implementation of an Simulation Data Provider.
	 */
	@Provides @Singleton
	SimulationDataProviderBatisImpl getSimulationDataProviderBatis() {
		return new SimulationDataProviderBatisImpl(Simulation.class);
	}
}
