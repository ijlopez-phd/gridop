package es.siani.gridop.injection;

import com.google.inject.Guice;
import com.google.inject.Injector;

import es.siani.energyagents.injection.PlatformExp01Module;
import es.siani.energyagents.injection.PlatformExp03Module;
import es.siani.energyagents.injection.PlatformExp04Module;

public class InjectionUtils {
	
	private static Injector _injector = null;
	
	
	/**
	 * Constructor.
	 */
	private InjectionUtils() {
		return;
	}
	
	
	/**
	 * Return an Injector.
	 */
	public static Injector injector() {
		
		if (_injector == null) {
			_injector = Guice.createInjector(
					new ConfigInjectionModule(),
					new DataInjectionModule(),
					new CommInjectionModule(),
					new PlatformExp04Module());
		}
		
		return _injector;
	}
}
