package es.siani.gridop.injection;

import com.google.inject.AbstractModule;
import com.google.inject.Singleton;

import es.siani.gridop.action.AspemServicesCaller;
import es.siani.gridop.action.AspemServicesCallerImpl;
import es.siani.gridop.xmpp.SimpleXmpp;
import es.siani.gridop.xmpp.SimpleXmppImpl;

public class CommInjectionModule extends AbstractModule {

	/**
	 * Dependency Injector module for the Communication objects.
	 */
	@Override
	protected void configure() {
		bind(SimpleXmpp.class).to(SimpleXmppImpl.class).in(Singleton.class);
		bind(AspemServicesCaller.class).to(AspemServicesCallerImpl.class).in(Singleton.class);
		
		return;
	}

}
