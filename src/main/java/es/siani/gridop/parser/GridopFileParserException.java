package es.siani.gridop.parser;

@SuppressWarnings("serial")
public class GridopFileParserException extends Exception {
	
	/**
	 * Constructor.
	 */
	public GridopFileParserException() {
		super();
	}
	
	
	/**
	 * Constructor.
	 */
	public GridopFileParserException(Throwable t) {
		super(t);
	}
	
	
	/**
	 * Constructor.
	 */
	public GridopFileParserException(String message) {
		super(message);
	}
	
	
	/**
	 * Constructor.
	 */
	public GridopFileParserException(String message, Throwable t) {
		super(message, t);
	}
}
