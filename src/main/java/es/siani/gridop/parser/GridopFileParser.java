package es.siani.gridop.parser;

import es.siani.gridop.model.GridopFile;

public interface GridopFileParser {
	
	/**
	 * Validates the string argument against a parser.
	 * @return GridopFile, if there is not parsing errors.
	 */
	public GridopFile validate(String content) throws GridopFileParserException;

}
