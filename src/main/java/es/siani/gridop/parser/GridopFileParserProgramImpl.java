package es.siani.gridop.parser;

import java.io.ByteArrayInputStream;

import es.siani.gridop.model.GridopFile;
import es.siani.simpledr.AsdrException;
import es.siani.simpledr.AsdrUtils;
import es.siani.simpledr.model.AsdrProgram;
import es.siani.simpledr.xml.XMLAsdrProgramHandler;
import es.siani.simpledr.xml.XMLAsdrProgramHandler20aImpl;

public class GridopFileParserProgramImpl implements GridopFileParser {
	
	protected XMLAsdrProgramHandler handler = new XMLAsdrProgramHandler20aImpl();
	
	
	/**
	 * Constructor.
	 */
	public GridopFileParserProgramImpl() {
		return;
	}
	
	
	/**
	 * @see GridopFileParser#validate(String)
	 */
	@Override
	public GridopFile validate(String content) throws GridopFileParserException {
		
		AsdrProgram program;
		try {
			program = AsdrUtils.createProgram(new ByteArrayInputStream(content.getBytes()));
		} catch (AsdrException ex) {
			throw new GridopFileParserException("Error occurred during parsing and validating the Content corresponding to a DR program.", ex);
		}
		
		return new GridopFile(program.getName(), content);
	}

}
