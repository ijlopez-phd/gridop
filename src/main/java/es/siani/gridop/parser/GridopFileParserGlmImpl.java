package es.siani.gridop.parser;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import es.siani.gridop.model.GridopFile;

public class GridopFileParserGlmImpl implements GridopFileParser {
	
	/** Reg expression for detecting the declaration of the 'agencyservices' module. */
	static final String REGEX_MODULE = "module\\s+agencyservices\\s*\\{";
	static final String REGEX_NODE_NAME = "nodeName\\s+(\\p{Alnum}+|'\\p{Alnum}+'|\"\\p{Alnum}+\");";

	
	/**
	 * Constructor.
	 */
	public GridopFileParserGlmImpl() {
		return;
	}
	
	
	/**
	 * @see GridopFileParser#validate(String)
	 */
	@Override
	public GridopFile validate(String content) throws GridopFileParserException {
		
		String glm = content.replaceAll("\n", "").replaceAll("\r", "");
		
		String nodeName = readNodeName(glm);
		
		return new GridopFile(nodeName, content);
	}
	
	
	/**
	 * Gets the name of the grid node described by the GLM file.
	 * 
	 * @param glm
	 *        The content of a valid GLM file.
	 * @return The name of the node described by the GLM file.
	 * @throws GridopFileParserException
	 */
	String readNodeName(String glm) throws GridopFileParserException {
		
		Matcher matcher = Pattern.compile(REGEX_MODULE).matcher(glm);
		if (matcher.find() == false) {
			throw new GridopFileParserException("The GLM does not contain the definition of the 'agencyservices' module.");
		}
		
		int idxBeginModule = matcher.start();
		int idxEndModule = this.findEndOfBlock(glm, idxBeginModule);
		
		matcher = Pattern.compile(REGEX_NODE_NAME).matcher(glm.substring(idxBeginModule, idxEndModule + 1));
		if (matcher.find() == false) {
			throw new GridopFileParserException("The GLM does not contain the sentence 'nodeName'.");
		}
		String nodeName = matcher.group(1);
		if (nodeName.startsWith("'") || nodeName.startsWith("\"")) {
			nodeName = nodeName.substring(1, nodeName.length() - 1);
		}
		
		return nodeName;
	}
	
	
	/**
	 * Find the end of a GLM block (delimited by braces) that starts at <code>idx</code>.
	 * @return
	 *          The index of the closing brace that closes the block,
	 *          <code>-1</code> if the block is not properly closed. 
	 */
	int findEndOfBlock(String glm, int idx) {
		
		int idxNextOpeningBrace = glm.indexOf("{", idx);
		int idxNextClosingBrace = glm.indexOf("}", idx);
		
		if ((idxNextOpeningBrace == -1) || (idxNextOpeningBrace > idxNextClosingBrace)) {
			return -1;
		}
		
		
		int idxClosingBrace = findEndOfBlock(glm, idxNextOpeningBrace + 1);
		
		return (idxClosingBrace == -1)? idxNextClosingBrace : glm.indexOf("}", idxClosingBrace + 1);
	}

}
