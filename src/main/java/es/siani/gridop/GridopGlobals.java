package es.siani.gridop;

public class GridopGlobals {
	
	// Configuration constants
	public static final String VAR_ENV_CONFIG_PATH = "GRIDOP_CONFIG";
	public static final String PARAM_SYS_CONFIG_PATH = "gridopConfig";
	public static final String INTERNAL_CONFIG_PATH = "config-gridop.properties";
	public static final String JADE_DF_NAME = "df.properties";
	
	// Datastore constants.
	public static final String STORE_DEFAULT_ENVIRONMENT = "development";
	
	
	// GUI constants
	public static final int TABLE_MAX_VISIBLE_ITEMS = 15;
	
	// Content
	public static final String ABOUT_RESOURCE = "about.html";
	public static final String CONTACT_RESOURCE = "contact.html";
}
