CREATE TABLE gridoperator (
   id serial NOT NULL, 
   description character varying(1024), 
   CONSTRAINT pk_gridoperator PRIMARY KEY (id)
);


CREATE TABLE scenario (
   id serial NOT NULL, 
   code character varying(64) NOT NULL, 
   store_name character varying(256) NOT NULL, 
   description character varying(1024), 
   program_name character varying(1024) NOT NULL, 
   program_content text NOT NULL, 
   CONSTRAINT pk_scenario PRIMARY KEY (id), 
   CONSTRAINT uq_sce_name UNIQUE (code)
);


CREATE TABLE program (
   id serial NOT NULL, 
   name character varying(1024) NOT NULL, 
   xml_definition text NOT NULL, 
   CONSTRAINT pk_program PRIMARY KEY (id)
);


CREATE TABLE simulation (
  id serial NOT NULL,
  name character varying(256),
  started timestamp without time zone,
  stopped timestamp without time zone,
  id_scenario integer NOT NULL,
  active boolean,
  CONSTRAINT pk_simulation PRIMARY KEY (id),
  CONSTRAINT fk_sim_scenario FOREIGN KEY (id_scenario)
      REFERENCES scenario (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE
);


CREATE TABLE aspem (
   id serial NOT NULL, 
   code character varying(128) NOT NULL, 
   name character varying(256) NOT NULL, 
   url character varying(2048) NOT NULL, 
   status boolean,
   CONSTRAINT pk_aspem PRIMARY KEY (id), 
   CONSTRAINT uq_asp_code UNIQUE (code), 
   CONSTRAINT uq_asp_url UNIQUE (url)
);


CREATE TABLE client (
   id serial NOT NULL, 
   aspemid integer NOT NULL, 
   code character varying(256) NOT NULL, 
   xmppaddress character varying(512) NOT NULL, 
   remote_xmppaddress character varying(512) NOT NULL, 
   CONSTRAINT pk_client PRIMARY KEY (id), 
   CONSTRAINT uq_cli_code UNIQUE (code), 
   CONSTRAINT uq_cli_xmppaddress UNIQUE (xmppaddress), 
   CONSTRAINT uq_cli_remote_address UNIQUE (remote_xmppaddress), 
   CONSTRAINT fk_cli_aspemid FOREIGN KEY (aspemid) REFERENCES aspem (id) ON UPDATE CASCADE ON DELETE CASCADE
);


CREATE TABLE client_event (
  id serial NOT NULL,
  clientid integer NOT NULL,
  stamp timestamp without time zone NOT NULL,
  description character varying(4096) NOT NULL,
  CONSTRAINT pk_client_event PRIMARY KEY (id),
  CONSTRAINT fk_cev FOREIGN KEY (clientid)
    REFERENCES client(id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE
);


CREATE TABLE clients_group (
  id serial NOT NULL,
  code character varying(128) NOT NULL,
  name character varying(256) NOT NULL,
  description character varying(1024),
  CONSTRAINT pk_clients_group PRIMARY KEY (id),
  CONSTRAINT uq_grp_code UNIQUE (code),
  CONSTRAINT uq_grp_name UNIQUE (name)
);


CREATE TABLE rel_client_group (
  client_id serial NOT NULL,
  clients_group_id integer NOT NULL,
  CONSTRAINT pk_real_client_group PRIMARY KEY (client_id, clients_group_id),
  CONSTRAINT fk_rcg_client FOREIGN KEY (client_id) REFERENCES client(id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE,
  CONSTRAINT fk_rcg_group FOREIGN KEY (clients_group_id) REFERENCES clients_group(id) ON DELETE CASCADE
);
