CREATE TABLE GRIDOPERATOR (
	ID INTEGER GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
	DESCRIPTION VARCHAR(1024));

CREATE TABLE SCENARIO (
	ID INTEGER GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
	CODE VARCHAR(64) NOT NULL,
	STORE_NAME VARCHAR(256),
	DESCRIPTION VARCHAR(1024),
	PROGRAM_NAME VARCHAR(1024) NOT NULL,
	PROGRAM_CONTENT CLOB(3M) NOT NULL,
	CONSTRAINT UQ_SCE_NAME UNIQUE (CODE));
	
CREATE TABLE PROGRAM (
	ID INTEGER GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
	NAME VARCHAR(1024) NOT NULL,
	XML_DEFINITION CLOB(3M) NOT NULL
);
	
CREATE TABLE SIMULATION (
	ID INTEGER GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
	NAME VARCHAR(256),
	STARTED TIMESTAMP,
	STOPPED TIMESTAMP,
	ID_SCENARIO INTEGER NOT NULL,
	ACTIVE BOOLEAN,
	CONSTRAINT FK_SIM_SCENARIO FOREIGN KEY (ID_SCENARIO) REFERENCES SCENARIO(ID) ON DELETE CASCADE);
	
CREATE TABLE ASPEM (
	ID INTEGER GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
	CODE VARCHAR(128) NOT NULL,
	NAME VARCHAR(256) NOT NULL,
	URL VARCHAR(2048) NOT NULL,
	STATUS BOOLEAN,
	CONSTRAINT UQ_ASP_CODE UNIQUE (CODE),
	CONSTRAINT UQ_ASP_URL UNIQUE (URL));
	
CREATE TABLE ASPEM_EVENT (
	ID INTEGER GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
	ASPEMID INTEGER NOT NULL,
	STAMP TIMESTAMP NOT NULL,
	DESCRIPTION VARCHAR(4096) NOT NULL,
	CONSTRAINT FK_AEV_ASPEM FOREIGN KEY (ASPEMID) REFERENCES ASPEM(ID) ON DELETE CASCADE);
	
CREATE TABLE CLIENT (
	ID INTEGER GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
	ASPEMID INTEGER NOT NULL,
	CODE VARCHAR(256) NOT NULL,
	XMPPADDRESS VARCHAR(512) NOT NULL,
	REMOTE_XMPPADDRESS VARCHAR(512) NOT NULL,
	CONSTRAINT UQ_CLI_CODE UNIQUE(CODE),
	CONSTRAINT UQ_CLI_XMPPADDRESS UNIQUE(XMPPADDRESS),
	CONSTRAINT UQ_CLI_REMOTE_XMPPADDRESS UNIQUE(REMOTE_XMPPADDRESS),
	CONSTRAINT FK_CLI_ASPEMID FOREIGN KEY (ASPEMID) REFERENCES ASPEM(ID) ON DELETE CASCADE);
	
CREATE TABLE CLIENT_EVENT (
	ID INTEGER GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
	CLIENTID INTEGER NOT NULL,
	STAMP TIMESTAMP NOT NULL,
	DESCRIPTION VARCHAR(4096) NOT NULL,
	CONSTRAINT FK_CEV FOREIGN KEY (CLIENTID) REFERENCES CLIENT(ID) ON DELETE CASCADE);
	
CREATE TABLE CLIENTS_GROUP (
	ID INTEGER GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
	CODE VARCHAR(128) NOT NULL,
	NAME VARCHAR(256) NOT NULL,
	DESCRIPTION VARCHAR(1024),
	CONSTRAINT UQ_GRP_CODE UNIQUE (CODE),
	CONSTRAINT UQ_GRP_NAME UNIQUE (NAME)
);

CREATE TABLE REL_CLIENT_GROUP (
	CLIENT_ID INTEGER NOT NULL,
	CLIENTS_GROUP_ID INTEGER NOT NULL,
	CONSTRAINT PK_RCG PRIMARY KEY (CLIENT_ID, CLIENTS_GROUP_ID),
	CONSTRAINT FK_RCG_CLIENT FOREIGN KEY (CLIENT_ID) REFERENCES CLIENT(ID) ON DELETE CASCADE,
	CONSTRAINT FK_RCG_GROUP  FOREIGN KEY (CLIENTS_GROUP_ID) REFERENCES CLIENTS_GROUP(ID) ON DELETE CASCADE
);