package es.siani.gridop.services;

import org.junit.Test;

import es.siani.gridop.GridopException;
import es.siani.gridop.model.Aspem;
import junit.framework.TestCase;

public class TestServicesHelper extends TestCase {
	
	
	/**
	 * Initialize resources used in each test.
	 */
	@Override
	public void setUp() throws Exception {
		super.setUp();
		return;
	}
	
	
	/**
	 * Close resources used in each test.
	 */
	@Override
	protected void tearDown() throws Exception {
		super.tearDown();
		return;
	}
	
	
	/**
	 * Test the conversion of an ASPEM to XML.
	 * @throws GridopException 
	 */
	@Test
	public void testAspemToXml() throws GridopException {
		
		Aspem aspem = new Aspem();
		aspem.setCode("aspem01");
		aspem.setHost("localhost");
		aspem.setPort(8080);
		
		String xml = ServicesHelper.ObjectToXML(aspem);
		
		Aspem aspemConv = ServicesHelper.XMLToObject(Aspem.class, xml);
		
		assertEquals(aspemConv.getCode(), aspem.getCode());
		assertEquals(aspemConv.getHost(), aspem.getHost());
		assertEquals(aspemConv.getPort(), aspem.getPort());
		
		return;
	}
	
}
