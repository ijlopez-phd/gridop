package es.siani.gridop.parser;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import junit.framework.TestCase;

import org.junit.Test;

import es.siani.gridop.model.GridopFile;
import static es.siani.gridop.parser.GridopFileParserGlmImpl.*;


public class TestFileParserGlmImpl extends TestCase {
	
	protected GridopFileParserGlmImpl parser;
	
	
	/**
	 * Setup.
	 */
	@Override
	public void setUp() throws Exception {
		this.parser = new GridopFileParserGlmImpl();
		return;
	}
	
	
	/**
	 * Checks the regular expression for getting the AgencyServices module's block
	 * of a GLM stream.
	 */
	@Test
	public void testRegexModule() {
		String MODULE = "module agencyservices {nodeName aa; scenarioCode aa; maxStepTime 60;}";
		String SAMPLE = "module residential; module tape;"
				+ MODULE
				+ "; object GridOperator { name gridop; servicesEndpoint localhost;};";
		
		Matcher matcher = Pattern.compile(REGEX_MODULE).matcher(SAMPLE);
		
		assertEquals(true, matcher.find());
		String block = SAMPLE.substring(
				matcher.start(),
				SAMPLE.indexOf("}", matcher.start()) + 1);
		assertEquals(MODULE, block);
		
		return;
	}
	
	
	/**
	 * Checks the regular expression for getting the AgencyServices module's name
	 * of a GLM stream.
	 */
	@Test
	public void testRegexNodeName() {
		
		String SAMPLE = "nodeName aaa;";
		Matcher matcher = Pattern.compile(REGEX_NODE_NAME).matcher(SAMPLE);
		
		assertEquals(1, matcher.groupCount());
		assertEquals(true, matcher.find());
		assertEquals("aaa", matcher.group(1));
		
		SAMPLE = "nodeName 'aaa';";
		matcher.reset(SAMPLE);
		assertEquals(true, matcher.find());
		assertEquals("'aaa'", matcher.group(1));
		
		SAMPLE = "nodeName \"aaa\";";		
		matcher.reset(SAMPLE);
		assertEquals(true, matcher.find());
		assertEquals("\"aaa\"", matcher.group(1));
		
		return;
	}
	
	
	/**
	 * Checks the searching process of the end of a GLM block.
	 */
	@Test
	public void testFindEndOfBlock() {
		
		this.parser = new GridopFileParserGlmImpl();
		
		String SAMPLE = "m{scenarioCode 'aaa';}"; // idx = 21
		assertEquals(21, parser.findEndOfBlock(SAMPLE, 0));
		
		SAMPLE = "m{a {code 'aaa';}}//comments"; // idx = 17;
		assertEquals(17, parser.findEndOfBlock(SAMPLE, 0));
		
		SAMPLE = "m{a {code 'aaa'; b {name 'aa'} mode 1;} // comments}"; // idx = 51
		assertEquals(51, parser.findEndOfBlock(SAMPLE, 0));
		
		SAMPLE = "m{a {code 'aaa'; b {name 'aa'} mode 1;} // comments"; // idx = -1
		assertEquals(-1, parser.findEndOfBlock(SAMPLE, 0));
		
		return;
	}
	
	
	/**
	 * Check the function that gets the name of a node described in a GLM file.
	 * @throws GridopFileParserException 
	 * 
	 */
	@Test
	public void testReadNodeName() throws IOException, GridopFileParserException {
		
		String glm = readFile(
				this.getClass().getResource("/glm/simple.glm").getFile(),
				Charset.forName("UTF-8"));
		
		String nodeName = this.parser.readNodeName(glm);
		assertEquals("aaa", nodeName);
		
		return;
	}
	
	
	/**
	 * Check the function that validates a GLM file and returns its description.
	 */
	@Test
	public void testValidate() throws IOException, GridopFileParserException {
		
		String glm = readFile(
				this.getClass().getResource("/glm/simple.glm").getFile(),
				Charset.forName("UTF-8"));
		
		GridopFile glmDescriptor = this.parser.validate(glm);
		
		assertEquals("aaa", glmDescriptor.getName());
		assertEquals(glm, glmDescriptor.getContent());
		
		return;
	}
	
	
	/**
	 * Read the content of a file into a String object.
	 */
	static String readFile(String path, Charset encoding) throws IOException {
		byte[] encoded = Files.readAllBytes(Paths.get(path));
		return encoding.decode(ByteBuffer.wrap(encoded)).toString();
	}	
	

}
